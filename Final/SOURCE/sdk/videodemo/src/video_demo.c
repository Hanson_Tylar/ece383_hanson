/************************************************************************/
/*																		*/
/*	video_demo.c	--	Nexys Video HDMI demonstration 						*/
/*																		*/
/************************************************************************/
/*	Author: Sam Bobrowicz												*/
/*	Copyright 2015, Digilent Inc.										*/
/************************************************************************/
/*  Module Description: 												*/
/*																		*/
/*		This file contains code for running a demonstration of the		*/
/*		Video input and output capabilities on the Nexys Video. It is a good	*/
/*		example of how to properly use the display_ctrl and				*/
/*		video_capture drivers.											*/
/*																		*/
/*																		*/
/************************************************************************/
/*  Revision History:													*/
/* 																		*/
/*		11/25/2015(SamB): Created										*/
/*		03/31/2017(ArtVVB): Updated sleep functions for 2016.4			*/
/*																		*/
/************************************************************************/

/* ------------------------------------------------------------ */
/*				Include File Definitions						*/
/* ------------------------------------------------------------ */

#include "video_demo.h"
#include "video_capture/video_capture.h"
#include "display_ctrl/display_ctrl.h"
#include "intc/intc.h"
#include <stdio.h>
#include "xuartlite_l.h"
//#include "xuartps.h"
#include "math.h"
#include <ctype.h>
#include <stdlib.h>
#include "xil_types.h"
#include "xil_cache.h"
#include "xparameters.h"
#include "sleep.h"
/*
 * XPAR redefines
 */
#define DYNCLK_BASEADDR XPAR_AXI_DYNCLK_0_BASEADDR
#define VGA_VDMA_ID XPAR_AXIVDMA_0_DEVICE_ID
#define DISP_VTC_ID XPAR_VTC_0_DEVICE_ID
#define VID_VTC_ID XPAR_VTC_1_DEVICE_ID
#define VID_GPIO_ID XPAR_AXI_GPIO_VIDEO_DEVICE_ID
#define VID_VTC_IRPT_ID XPAR_INTC_0_VTC_1_VEC_ID
#define VID_GPIO_IRPT_ID XPAR_INTC_0_GPIO_0_VEC_ID
#define SCU_TIMER_ID XPAR_AXI_TIMER_0_DEVICE_ID
#define UART_BASEADDR XPAR_UARTLITE_0_BASEADDR

/* ------------------------------------------------------------ */
/*				Global Variables								*/
/* ------------------------------------------------------------ */

/*
 * Display and Video Driver structs
 */
DisplayCtrl dispCtrl;
XAxiVdma vdma;
VideoCapture videoCapt;
INTC intc;
char fRefresh; //flag used to trigger a refresh of the Menu on video detect

/*
 * Framebuffers for video data
 */
u8 frameBuf[DISPLAY_NUM_FRAMES][DEMO_MAX_FRAME];
u8 *pFrames[DISPLAY_NUM_FRAMES]; //array of pointers to the frame buffers

/*
 * Interrupt vector table
 */
const ivt_t ivt[] = {
	videoGpioIvt(VID_GPIO_IRPT_ID, &videoCapt),
	videoVtcIvt(VID_VTC_IRPT_ID, &(videoCapt.vtc))
};

/* ------------------------------------------------------------ */
/*				Procedure Definitions							*/
/* ------------------------------------------------------------ */

int main(void)
{
	Xil_ICacheEnable();
	Xil_DCacheEnable();

	DemoInitialize();

	DemoRun();

	return 0;
}


void DemoInitialize()
{
	int Status;
	XAxiVdma_Config *vdmaConfig;
	int i;

	/*
	 * Initialize an array of pointers to the 3 frame buffers
	 */
	for (i = 0; i < DISPLAY_NUM_FRAMES; i++)
	{
		pFrames[i] = frameBuf[i];
	}

	/*
	 * Initialize VDMA driver
	 */
	vdmaConfig = XAxiVdma_LookupConfig(VGA_VDMA_ID);
	if (!vdmaConfig)
	{
		xil_printf("No video DMA found for ID %d\r\n", VGA_VDMA_ID);
		return;
	}
	Status = XAxiVdma_CfgInitialize(&vdma, vdmaConfig, vdmaConfig->BaseAddress);
	if (Status != XST_SUCCESS)
	{
		xil_printf("VDMA Configuration Initialization failed %d\r\n", Status);
		return;
	}

	/*
	 * Initialize the Display controller and start it
	 */
	Status = DisplayInitialize(&dispCtrl, &vdma, DISP_VTC_ID, DYNCLK_BASEADDR, pFrames, DEMO_STRIDE);
	if (Status != XST_SUCCESS)
	{
		xil_printf("Display Ctrl initialization failed during demo initialization%d\r\n", Status);
		return;
	}
	Status = DisplayStart(&dispCtrl);
	if (Status != XST_SUCCESS)
	{
		xil_printf("Couldn't start display during demo initialization%d\r\n", Status);
		return;
	}

	/*
	 * Initialize the Interrupt controller and start it.
	 */
	Status = fnInitInterruptController(&intc);
	if(Status != XST_SUCCESS) {
		xil_printf("Error initializing interrupts");
		return;
	}
	fnEnableInterrupts(&intc, &ivt[0], sizeof(ivt)/sizeof(ivt[0]));

	/*
	 * Initialize the Video Capture device
	 */
	Status = VideoInitialize(&videoCapt, &intc, &vdma, VID_GPIO_ID, VID_VTC_ID, VID_VTC_IRPT_ID, pFrames, DEMO_STRIDE, DEMO_START_ON_DET);
	if (Status != XST_SUCCESS)
	{
		xil_printf("Video Ctrl initialization failed during demo initialization%d\r\n", Status);
		return;
	}

	/*
	 * Set the Video Detect callback to trigger the menu to reset, displaying the new detected resolution
	 */
	VideoSetCallback(&videoCapt, DemoISR, &fRefresh);
	return;
}

u32 xPlayer, yPlayer;
//u32 xPlayer_2, yPlayer_2;
u32 xLeft, xRight;
u32 yTop, yBottom;
int xVel, yVel;//, xVel_2;//, yVel_2;
int playerTrail[640][480];// playerTrail_2[640][480];
int GAMEOVER578 = 1;

void DemoRun()
{
	char userInput = 'r';
	u32 locked;
	XGpio *GpioPtr = &videoCapt.gpio;

	xLeft = dispCtrl.vMode.width/8;
	xRight = dispCtrl.vMode.width*3-xLeft;
	yTop = dispCtrl.vMode.height/8;
	yBottom = dispCtrl.vMode.height - yTop;
	xPlayer = xLeft*2;
	yPlayer = yTop*2;
//	xPlayer_2 = xRight - xLeft*2;
//	yPlayer_2 = yBottom - yTop*2;
	xVel = 30;
	yVel = 0;
//	xVel_2 = -30;
//	yVel_2 = 0;

	/* Flush UART FIFO */
	while (!XUartLite_IsReceiveEmpty(UART_BASEADDR))
	{
		XUartLite_ReadReg(UART_BASEADDR, XUL_RX_FIFO_OFFSET);
	}

	while (1)
	{
		DemoPrintMenu();
		fRefresh = 0;

		/* Wait for data on UART */
//		while (XUartLite_IsReceiveEmpty(UART_BASEADDR) && !fRefresh){}

		/* Store the first character in the UART receive FIFO and echo it */
		if (!XUartLite_IsReceiveEmpty(UART_BASEADDR)){
			userInput = XUartLite_ReadReg(UART_BASEADDR, XUL_RX_FIFO_OFFSET);
		}
		else{  //Refresh triggered by video detect interrupt
			userInput = 'r';
		}

		switch (userInput){
		case '1':
			DemoChangeRes();
			break;
		case '2':
			GAMEOVER578 = 0;
			break;
		case 'w':
			xVel = 0;
			yVel = -8;
			break;
		case 'a':
			xVel = -30;
			yVel = 0;
			break;
		case 's':
			xVel = 0;
			yVel = 8;
			break;
		case 'd':
			xVel = 30;
			yVel = 0;
			break;
//		case 'i':
//			xVel_2 = 0;
//			yVel_2 = -8;
//			break;
//		case 'j':
//			xVel_2 = -30;
//			yVel_2 = 0;
//			break;
//		case 'k':
//			xVel_2 = 0;
//			yVel_2 = 8;
//			break;
//		case 'l':
//			xVel_2 = 30;
//			yVel_2 = 0;
//			break;
		case 'r':
			locked = XGpio_DiscreteRead(GpioPtr, 2);
			xil_printf("%d", locked);
			break;
		default :
			xil_printf("\n\rInvalid Selection");
			usleep(50000);
		}
		if(GAMEOVER578==1){
			Tron_OVER(pFrames[dispCtrl.curFrame], dispCtrl.vMode.width, dispCtrl.vMode.height, DEMO_STRIDE);
			xil_printf("GAMEOVER");
		}
		else{
			usleep(100);
			Tron(pFrames[dispCtrl.curFrame], dispCtrl.vMode.width, dispCtrl.vMode.height, DEMO_STRIDE);
		}
	}

	return;
}

void DemoPrintMenu()
{
	xil_printf("\x1B[H"); //Set cursor to top left of terminal
	xil_printf("\x1B[2J"); //Clear terminal
	xil_printf("**************************************************\n\r");
	xil_printf("*               Tron Light Cycles                *\n\r");
	xil_printf("**************************************************\n\r");
	xil_printf("\n\r");
	xil_printf("1 - Change Display Resolution\n\r");
	xil_printf("2 - Start Tron Game\n\r");
	xil_printf("\tWASD to Control Player 1 and IJKL for player 2\n\r");
	xil_printf("\n\r");
	xil_printf("\n\r");
	xil_printf("Enter a selection:");
}

void DemoChangeRes()
{
	int fResSet = 0;
	int status;
	char userInput = 0;

	/* Flush UART FIFO */
	while (!XUartLite_IsReceiveEmpty(UART_BASEADDR)){
		XUartLite_ReadReg(UART_BASEADDR, XUL_RX_FIFO_OFFSET);
	}

	while (!fResSet){
		DemoCRMenu();

		/* Wait for data on UART */
		while (XUartLite_IsReceiveEmpty(UART_BASEADDR) && !fRefresh){}

		/* Store the first character in the UART recieve FIFO and echo it */

		userInput = XUartLite_ReadReg(UART_BASEADDR, XUL_RX_FIFO_OFFSET);
		xil_printf("%c", userInput);
		status = XST_SUCCESS;
		switch (userInput){
		case '1':
			status = DisplayStop(&dispCtrl);
			DisplaySetMode(&dispCtrl, &VMODE_640x480);
			DisplayStart(&dispCtrl);
			fResSet = 1;
			break;
		case '2':
			status = DisplayStop(&dispCtrl);
			DisplaySetMode(&dispCtrl, &VMODE_800x600);
			DisplayStart(&dispCtrl);
			fResSet = 1;
			break;
		case '3':
			status = DisplayStop(&dispCtrl);
			DisplaySetMode(&dispCtrl, &VMODE_1280x720);
			DisplayStart(&dispCtrl);
			fResSet = 1;
			break;
		case '4':
			status = DisplayStop(&dispCtrl);
			DisplaySetMode(&dispCtrl, &VMODE_1280x1024);
			DisplayStart(&dispCtrl);
			fResSet = 1;
			break;
		case '5':
			status = DisplayStop(&dispCtrl);
			DisplaySetMode(&dispCtrl, &VMODE_1920x1080);
			DisplayStart(&dispCtrl);
			fResSet = 1;
			break;
		case 'q':
			fResSet = 1;
			break;
		default :
			xil_printf("\n\rInvalid Selection");
			usleep(50000);
		}
		if (status == XST_DMA_ERROR)
		{
			xil_printf("\n\rWARNING: AXI VDMA Error detected and cleared\n\r");
		}
	}
}

void DemoCRMenu() {
	xil_printf("\x1B[H"); //Set cursor to top left of terminal
	xil_printf("\x1B[2J"); //Clear terminal
	xil_printf("**************************************************\n\r");
	xil_printf("*             Nexys Video HDMI Demo              *\n\r");
	xil_printf("**************************************************\n\r");
	xil_printf("*Current Resolution: %28s*\n\r", dispCtrl.vMode.label);
	printf("*Pixel Clock Freq. (MHz): %23.3f*\n\r", dispCtrl.pxlFreq);
	xil_printf("**************************************************\n\r");
	xil_printf("\n\r");
	xil_printf("1 - %s\n\r", VMODE_640x480.label);
	xil_printf("2 - %s\n\r", VMODE_800x600.label);
	xil_printf("3 - %s\n\r", VMODE_1280x720.label);
	xil_printf("4 - %s\n\r", VMODE_1280x1024.label);
	xil_printf("5 - %s\n\r", VMODE_1920x1080.label);
	xil_printf("q - Quit (don't change resolution)\n\r");
	xil_printf("\n\r");
	xil_printf("Select a new resolution:");
}

void Tron(u8 *frame, u32 width, u32 height, u32 stride){
	u32 iPixelAddr;
	u8 wRed, wBlue, wGreen;
	u32 xcoi, ycoi;
	if(yVel==0){
		for(int x = xPlayer-16; x <= xPlayer+16; x++){
			for(int y = yPlayer-1; y <= yPlayer+1; y++){
				playerTrail[x][y] = 1;
			}
		}
	}
	else{
		for(int x = xPlayer-4; x <= xPlayer+4; x++){
			for(int y = yPlayer-4; y <= yPlayer+5; y++){
				playerTrail[x][y] = 1;
			}
		}
	}
//	if(yVel_2==0){
//		for(int x = xPlayer_2-16; x <= xPlayer_2+16; x++){
//			for(int y = yPlayer_2-1; y <= yPlayer_2+1; y++){
//				playerTrail_2[x][y] = 1;
//			}
//		}
//	}
//	else{
//		for(int x = xPlayer_2-4; x <= xPlayer_2+4; x++){
//			for(int y = yPlayer_2-4; y <= yPlayer_2+5; y++){
//				playerTrail_2[x][y] = 1;
//			}
//		}
//	}
	xPlayer += xVel;
	yPlayer += yVel;
//	xPlayer_2 += xVel;
//	yPlayer_2 += yVel;
	if(playerTrail[xPlayer][yPlayer] == 1){
//	if(playerTrail[xPlayer][yPlayer] == 1 || playerTrail_2[xPlayer_2][yPlayer_2] == 1 || playerTrail_2[xPlayer][yPlayer] == 1 || playerTrail[xPlayer_2][yPlayer_2] == 1){
		GAMEOVER578 = 1;
	}
	for(xcoi = 0; xcoi<width*3; xcoi+=3){
		iPixelAddr = xcoi;
		for(ycoi = 0; ycoi < height; ycoi++){
			if (xcoi < xLeft || xcoi > xRight){
				wRed = 0;
				wGreen = 128;
				wBlue = 128;
			}
			else if(ycoi < yTop || ycoi > yBottom){
				wRed = 0;
				wGreen = 128;
				wBlue = 128;
			}
			else if((xcoi >= xPlayer-16 && xcoi <= xPlayer+16) && (ycoi >= yPlayer-4 && ycoi <= yPlayer+4)){
				wRed = 0;
				wGreen = 255;
				wBlue = 127;
			}
//			else if((xcoi >= xPlayer_2-16 && xcoi <= xPlayer_2+16) && (ycoi >= yPlayer_2-4 && ycoi <= yPlayer_2+4)){
//				wRed = 255;
//				wGreen = 0;
//				wBlue = 0;
//			}
			else if(playerTrail[xcoi][ycoi] == 1){
//			else if(playerTrail[xcoi][ycoi] == 1 || playerTrail_2[xcoi][ycoi] == 1){
				wRed = 255;
				wGreen = 255;
				wBlue = 255;
			}
			else{
				wRed = 0;
				wGreen = 0;
				wBlue = 0;
			}

			frame[iPixelAddr] = wGreen;
			frame[iPixelAddr + 1] = wBlue;
			frame[iPixelAddr + 2] = wRed;
			iPixelAddr += stride;
		}
		}
}

void Tron_OVER(u8 *frame, u32 width, u32 height, u32 stride){
	u32 iPixelAddr;
	u8 wRed, wBlue, wGreen;
	u32 xcoi, ycoi;
	for(xcoi = 0; xcoi<width*3; xcoi+=3){
		iPixelAddr = xcoi;
		for(ycoi = 0; ycoi < height; ycoi++){
			if (xcoi < xLeft || xcoi > xRight){
				wRed = 0;
				wGreen = 128;
				wBlue = 128;
			}
			else if(ycoi < yTop || ycoi > yBottom){
				wRed = 0;
				wGreen = 128;
				wBlue = 128;
			}
			else{
				wRed = 100;
				wGreen = 0;
				wBlue = 200;
			}

			frame[iPixelAddr] = wGreen;
			frame[iPixelAddr + 1] = wBlue;
			frame[iPixelAddr + 2] = wRed;
			iPixelAddr += stride;
		}
		}
}
void DemoISR(void *callBackRef, void *pVideo)
{
	char *data = (char *) callBackRef;
	*data = 1; //set fRefresh to 1
}


