library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity lec18_tb is
end lec18_tb;

architecture Behavioral of lec18_tb is

    component lec10 is
        generic (N: integer := 4);
        Port(	
            clk: in  STD_LOGIC;
            reset_n : in  STD_LOGIC;
            ctrl: in std_logic_vector(1 downto 0);
            D: in unsigned (N-1 downto 0);
            Q: out unsigned (N-1 downto 0);
            roll : out STD_LOGIC);
    end component;

    signal clk : std_logic := '0';
    signal reset_n : std_logic := '0';
    signal ctrl : STD_LOGIC_VECTOR(1 downto 0) := (others => '0');
    signal D : unsigned (3 downto 0) := (others => '0');
    signal Q : unsigned (3 downto 0);
    signal roll : STD_LOGIC;
    
    constant clk_period : time := 10 ns;
    
begin

    uut : lec10 
    generic map (4)
    port map (
        clk => clk,
        reset_n => reset_n,
        ctrl => ctrl,
        D => D,
        Q => Q,
        roll => roll);
        
    clk_process :process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;

    reset_n <= '0', '1' after clk_period;
    ctrl <= "00", "01" after clk_period*3;
    
end Behavioral;
