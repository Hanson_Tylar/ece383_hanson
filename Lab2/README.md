# Lab 2 - Data Acquisition, Storage and Display

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Hardware schematic](#hardware-schematic)
4. [Debugging](#debugging)
5. [Testing methodology or results](#testing-methodology-or-results)
6. [Lab Capabilities](#Lab-Capabilities)
7. [Observations and Conclusions](#observations-and-conclusions)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
In this lab, I will integrate the video display controller developed in Lab 1 with the audio codec on the NEXYS board to build a basic 2-channel oscilloscope. When complete, this lab should generate an output similar to the picture below.

- Required Functionality
	- Get a single channel of the oscilloscope to display with reliable triggering that holds the waveform at a single point on the left edge of the display.
- B Functionality
	- Add a second channel (in green).
	- Integrate the button debouncing strategy in HW #7 to debounce the buttons controlling the trigger time and trigger voltage.
	- Move the cursors on the screen.
- A Functionality
	- Use the trigger voltage marker to establish the actual trigger voltage used to capture the waveform. As the trigger is moved up and down, you should see the point at which the waveform intersects the left side of the screen change.

![](images/objective.jpg)

### Preliminary design
Lab 2 is broken down into a bunch of separate modules, shown in the block diagram below. Preliminary design began with porting over components from lab one and tying them together in the lab two data path. UPDATE THIS DIAGRAM IN PPTX

![](images/lab2_block_diagram.jpg)

#### Datapath
The datapath takes care of the math and bit manipulation of all the signals. It tells the control module when the write address counter has reached 1023, when the audio codec is ready to give data, and when to trigger. It also controls the channel inputs to the video entity. 

#### Control
The control FSM controls when data coming from the audio codec wrapper should be written into memory and what address to write to. 

![](images/State_diagram.jpg)
##### Control Unit Waveform
This waveform shows the CU starting in the wait for trigger state, receiving a trigger, and writing two samples into memory before waiting for another trigger.
![](images/fsm_tb.JPG)

#### Flag Register
The flag register will interface our lab2 component with a MicroBlaze as follows: The LAB2 component will produce some data, put it on a data line to the MicroBlaze, and then set one of the bits of the flag register. Then, the MicroBlaze will, at some point, look at the flag register bit. When it sees that the 'set' bit is 1, the MicroBlaze will grab the data from the register and clear the set bit. These are just like the flag bits on the MSP 430.
##### Flag Register Waveforms
These images show each bit of the output being set then cleared. 
![](images/flag_reg_1.JPG)

![](images/flag_reg_2.JPG)

### Hardware schematic
![](images/lab2Connections_v2.jpg)

### Debugging
While working on gate check one I kept getting black box entity errors during implementation. After removing the flag register and control unit instantiations I was able to complete gate check one. I was able to get a waveform on the screen but the triggering was not reliable. After changing the current_level and prevous_level signals to 10 bits to match the trigger_volt the waveform displayed steadily. 

### Testing methodology or results
#### Gate Check 1
Audio signals are looping through the in / out ports and oscilloscope display still works.
![](images/GC1.jpg)

### Gate Check 2, Required, B, and A Functionality
This video shows channel 1 in yellow and channel 2 in green. The scope is triggering off of channel one.

[Lab Functionality](https://youtu.be/K3htgtx7rJc "Lab Functionality")

#### Lab Capabilities
1. The horizontal axis represents time. There are 10 major divisions on the display; how long does each major division represent?
	1. Period / # Divisions = 4.55 ms / 5 = 909.1 us per major division
	2. 220 Hz is 15 minor divisions
2. Each major time division is split into 4 minor division, how long does each minor division represent?
	1. #1 / 4 = 227.3 us
3. Generate a sine wave that can be fully captured on your display (like the yellow channel in the image at the top of this web page). record its height in major and minor vertical divisions. Measure this same audio output using the break out audio cable. Record the peak-to-peak voltage. Compute the number of volts in each major and minor vertical division.
	1. pk-pk divisions = 6
	2. Oscope pk-pk = # 1.11V
	3. volt per major div = #2/#1 = .185 volts per division
	4. volt per minor division = #3/4 = 46.25 mV / division
4. Starting at address 0, how long does it take to fill the entire memory with audio samples (coming in at 48kHz)?
	1. Wait trigger - 1 cycle
	2. Wait ready - 1 cycle
	3. write BRAM - 1 cycle
	4. BRAM Full - 1 cycle
		1. 1+1024*(1+1)+1 = 2050 cycles
		2. 20.83 us / cycle * 2050 cycles = 42.71 ms
5. How long does it take to completely draw the display once?
	1. The read clock on the BRAM is the same as the system clock at 100 MHz.
	2. The O-scope display is 620 pixels wide.
	3. 10 ns / cycle * 620 = 6.2 us
6. The question is likely relevant to Lab 3 - how long is the vsynch signal held low? - 0.06 ms. From HW#5

### Observations and Conclusions
The purpose of this lab was to create an oscilloscope face that can display a steady waveform on two channels. I completed every level of functionality described in this lab. I learned many things doing this lab. I learned a little bit about the differences between signed, unsigned, and standard logic vector but am still very confused about how we used them in this lab. I learned how a real oscilloscope determines when to start drawing a waveform. I learned how to write to and read from block ram.

### Documentation
None