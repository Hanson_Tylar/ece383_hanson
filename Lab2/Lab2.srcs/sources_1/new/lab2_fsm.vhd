--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 6 Feb 17
-- Course:  ECE 383
-- File: lab2_fsm.vhdl
-- HW:  Lab2
--
-- Purp: Control Unit for Lab 2. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;
entity lab2_fsm is
    port ( 
    clk : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    sw : in STD_LOGIC_VECTOR(2 downto 0);
    cw : out STD_LOGIC_VECTOR(2 downto 0));
end lab2_fsm;

architecture Behavioral of lab2_fsm is
    type state_type is( WaitTrigger, WaitReady, WriteBRAM, BRAMFull);
        signal state: state_type := WaitTrigger;

begin
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                state <= WaitTrigger;
            else
                case state is
                    when WaitTrigger =>
                        cw <= "010";
                        if(sw(2) = '1') then state <= WaitReady; end if;
                    when WaitReady =>
                        cw <= "010";
                        if(sw(1) = '1') then state <= WriteBRAM; end if;
                    when WriteBRAM =>
                        cw <= "101";
                        if(sw(0) = '1') then state <= BRAMFull;
                        else state <= WaitReady; end if;
                    when BRAMFull =>
                        cw <= "011";
                        state <= WaitTrigger;
                end case;
            end if;
        end if;
    end process;

end Behavioral;