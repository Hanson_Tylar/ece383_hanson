--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 6 Feb 17
-- Course:  ECE 383
-- File: lab2_datapath.vhdl
-- HW:  Lab2
--
-- Purp: Datapath for Lab 2. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;

entity lab2_datapath is
    Port(
	clk : in  STD_LOGIC;
	reset_n : in  STD_LOGIC;
	ac_mclk : out STD_LOGIC;
	ac_adc_sdata : in STD_LOGIC;
	ac_dac_sdata : out STD_LOGIC;
	ac_bclk : out STD_LOGIC;
	ac_lrclk : out STD_LOGIC;
	scl : inout STD_LOGIC;
	sda : inout STD_LOGIC;	
	tmds : out  STD_LOGIC_VECTOR (3 downto 0);
	tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
	sw: out std_logic_vector(2 downto 0);
	cw: in std_logic_vector (2 downto 0);
	btn: in	STD_LOGIC_VECTOR(4 downto 0);
	exWrAddr: in std_logic_vector(9 downto 0);
	exWen, exSel: in std_logic;
	Lbus_out, Rbus_out: out std_logic_vector(15 downto 0);
	exLbus, exRbus: in std_logic_vector(15 downto 0);
	flagQ: out std_logic_vector(7 downto 0);
	flagClear: in std_logic_vector(7 downto 0));
end lab2_datapath;

architecture Behavioral of lab2_datapath is

signal trigger_time: unsigned(9 downto 0) := to_unsigned(320,10);
signal trigger_volt: unsigned(9 downto 0) := to_unsigned(220,10);
signal row, column, current_level, previous_level, write_cntr: unsigned(9 downto 0):= (others => '0');
signal ch1, ch2, ready, reset, wrEnb_sig, write_cntr_cmp, v_synch : STD_LOGIC := '0';
signal old_button, button_activity: std_logic_vector(4 downto 0):= (others => '0');
signal L_bus_in, R_Bus_in, LBus_out_sig, RBus_out_sig, dataIn_sig, dataIn_sig_R, readL, readR : std_logic_vector(17 downto 0) := (others => '0');
signal set : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
signal wrAddr_sig : STD_LOGIC_VECTOR(9 downto 0) := (others => '0');
signal sign2Unsign_out_sig, sign2Unsign_out_sig_R : unsigned(17 downto 0) := (others => '0');
type state_type is (WaitBtnC, WaitOther, IncVolt, IncTime, DecVolt, DecTime);
signal state: state_type := WaitBtnC;

begin

    loopback : process (clk)
    begin
        if (rising_edge(clk)) then
            if reset_n = '0' then
                L_bus_in <= (others => '0');
                R_bus_in <= (others => '0');                
            elsif(ready = '1') then
                previous_level <= current_level;
                L_bus_in <= LBus_out_sig;
                R_bus_in <= RBus_out_sig;
            end if;
        end if;
    end process;
    
    write_counter : process(clk)
    begin
        if(rising_edge(clk)) then
            if cw(1 downto 0) = "11" then  -- reset
                write_cntr <= to_unsigned(23,10);
            elsif cw(1 downto 0) = "10" then -- hold
                write_cntr <= write_cntr;
            elsif cw(1 downto 0) = "01" then
                if write_cntr < 1023 then -- count up
                    write_cntr <= write_cntr + 1;
                    write_cntr_cmp <= '0';
                else
                    write_cntr_cmp <= '1'; -- Signal BRAM FUll
                end if;            
            end if;
        end if;
    end process;
    
    button_fsm : process(clk)
        begin
            if (rising_edge(clk)) then
                if(reset_n = '0') then
                    state <= WaitBtnC;
                else
                    case state is
                        when WaitBtnC =>
                            if btn(4) = '1' then state <= WaitOther; end if;
                        when WaitOther =>
                            if btn(1) = '1' then state <= DecTime;
                            elsif btn(3) = '1' then state <= IncTime;
                            elsif btn(2) = '1' then state <= IncVolt;
                            elsif btn(0) = '1' then state <= DecVolt;
                            end if;
                        when DecTime =>
                            if trigger_time >= 35 then
                                trigger_time <= trigger_time - 15; end if;
                            state <= WaitBtnC;
                        when IncTime =>
                            if trigger_time <= 605 then
                                trigger_time <= trigger_time + 15; end if;
                            state <= WaitBtnC;
                        when DecVolt =>
                            if trigger_volt >= 30 then
                                trigger_volt <= trigger_volt - 10; end if;
                            state <= WaitBtnC;
                        when IncVolt =>
                            if trigger_volt <= 410 then
                                trigger_volt <= trigger_volt + 10; end if;
                            state <= WaitBtnC;
                    end case;
                end if;
            end if;
        end process;
    
    current_level <= sign2Unsign_out_sig(17 downto 8)-803;
    sw(2) <= '1' when current_level = trigger_volt and previous_level > trigger_volt else '0';
    sw(1) <= ready;
    sw(0) <= write_cntr_cmp;
    reset <= not reset_n;

    -- BRAM_SDP Signal Inputs
    wrEnb_sig <= exWen when exSel = '1' else cw(2);
    dataIn_sig <= (exLBus & "00") when exSel = '1' else STD_LOGIC_VECTOR(sign2Unsign_out_sig);
    dataIn_sig_R <= (exLBus & "00") when exSel = '1' else STD_LOGIC_VECTOR(sign2Unsign_out_sig_R);
    wrAddr_sig <= STD_LOGIC_VECTOR(write_cntr) when exSel = '0' else exWrAddr;
    
    sign2Unsign_out_sig <= unsigned(L_Bus_in);
    sign2Unsign_out_sig_R <= unsigned(R_Bus_in);
    
    ch1 <= '1' when row = unsigned(readL(17 downto 8))-803 and column >= 23 and column <= 620 else '0';
    ch2 <= '1' when row = unsigned(readR(17 downto 8))-803 and column >= 23 and column <= 620 else '0';
        
    inst_video : video port map(
        clk => clk,
        reset_n => reset_n,
        tmds => tmds,
        tmdsb => tmdsb,
        trigger_time => trigger_time,
        trigger_volt => trigger_volt,
        row => row,
        column => column,
        ch1 => ch1,
        ch1_enb => '1',
        ch2 => ch2,
        ch2_enb => '1',
        v_synch => v_synch );
        
    inst_audio_codec_wrapper : Audio_Codec_Wrapper port map(
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => ready,
        L_bus_in => L_Bus_in, -- left channel input to DAC
        R_bus_in => R_Bus_in, -- right channel input to DAC
        L_bus_out => LBus_out_sig, -- left channel output from ADC
        R_bus_out => RBus_out_sig, -- right channel output from ADC
        scl => scl,
        sda => sda );
    
    set <= "00000"&ready&write_cntr_cmp&v_synch;
            
inst_flagRegister : flagRegister 
    generic map(8)
    port map(
        clk => clk,
        reset_n => reset_n,
        set => set,
        clear => flagClear,
        Q => flagQ );

    BRAM_SDP: BRAM_SDP_MACRO
	generic map (
		BRAM_SIZE => "18Kb", 			-- Target BRAM, "18Kb" or "36Kb"
		DEVICE => "7SERIES", 			-- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
		DO_REG => 0, 					-- Optional output register disabled
		INIT => X"000000000000000000",	-- Initial values on output port
		INIT_FILE => "NONE",			-- Not sure how to initialize the RAM from a file
		WRITE_WIDTH => 18, 				-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
		READ_WIDTH => 18, 				-- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
		SIM_COLLISION_CHECK => "NONE", 	-- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
		SRVAL => X"000000000000000000")	-- Set/Reset value for port output
	port map (
		DO => readL,				    -- Output read data port, width defined by READ_WIDTH parameter
		RDADDR => STD_LOGIC_VECTOR(column),	-- Input address, width defined by port depth
		RDCLK => clk,	 				-- 1-bit input clock
		RST => reset,					-- active high reset
		RDEN => '1',					-- read enable 
		REGCE => '1',					-- 1-bit input read output register enable - ignored
		DI => dataIn_sig,				-- Input data port, width defined by WRITE_WIDTH parameter
		WE => "11",			            -- since RAM is byte read, this determines high or low byte
		WRADDR => wrAddr_sig,			-- Input write address, width defined by write port depth
		WRCLK => clk,		 			-- 1-bit input write clock
		WREN => wrEnb_sig);				-- 1-bit input write port enable
		
    BRAM_SDP_R: BRAM_SDP_MACRO
    generic map (
        BRAM_SIZE => "18Kb",             -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",             -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6, 7SERIES"
        DO_REG => 0,                     -- Optional output register disabled
        INIT => X"000000000000000000",    -- Initial values on output port
        INIT_FILE => "NONE",            -- Not sure how to initialize the RAM from a file
        WRITE_WIDTH => 18,                 -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 18,                 -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        SIM_COLLISION_CHECK => "NONE",     -- Collision check enable "ALL", "WARNING_ONLY", "GENERATE_X_ONLY" or "NONE"
        SRVAL => X"000000000000000000")    -- Set/Reset value for port output
    port map (
        DO => readR,                    -- Output read data port, width defined by READ_WIDTH parameter
        RDADDR => STD_LOGIC_VECTOR(column),    -- Input address, width defined by port depth
        RDCLK => clk,                     -- 1-bit input clock
        RST => reset,                    -- active high reset
        RDEN => '1',                    -- read enable 
        REGCE => '1',                    -- 1-bit input read output register enable - ignored
        DI => dataIn_sig_R,                -- Input data port, width defined by WRITE_WIDTH parameter
        WE => "11",                        -- since RAM is byte read, this determines high or low byte
        WRADDR => wrAddr_sig,            -- Input write address, width defined by write port depth
        WRCLK => clk,                     -- 1-bit input write clock
        WREN => wrEnb_sig);                -- 1-bit input write port enable
        
end Behavioral;