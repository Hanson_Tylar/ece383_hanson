--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 14 Feb 17
-- Course:  ECE 383
-- File: flagregister_tb.vhdl
-- HW:  Lab2
--
-- Purp: Test the implementation of Flag register for Lab 2. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.lab2Parts.all;	

entity flagregister_tb is
end flagregister_tb;

architecture Behavioral of flagregister_tb is

signal clk, reset_n : STD_LOGIC := '0';
signal set, clear, Q : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
constant clk_period : time := 10 ns;

begin

uut : flagRegister
    Generic map(8)
    Port map(	
        clk => clk,
        reset_n => reset_n,
        set => set,
        clear => clear,
        Q => Q);

-- Clock process definitions
clk_process :process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
end process;

    reset_n <= '1' after clk_period*1;
    set <= x"00" after clk_period*3, x"01" after clk_period*5, x"02" after clk_period*7, x"04" after clk_period*9, x"08" after clk_period*11, x"10" after clk_period*13, x"20" after clk_period*15, x"40" after clk_period*17, x"80" after clk_period*19, x"00" after clk_period*21;
    clear <= x"00" after clk_period*23, x"01" after clk_period*25, x"02" after clk_period*27, x"04" after clk_period*29, x"08" after clk_period*31, x"10" after clk_period*33, x"20" after clk_period*35, x"40" after clk_period*37, x"80" after clk_period*39, x"00" after clk_period*41;
end Behavioral;
