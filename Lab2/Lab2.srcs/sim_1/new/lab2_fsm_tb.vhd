--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 14 Feb 17
-- Course:  ECE 383
-- File: lab2_fsm_tb.vhdl
-- HW:  Lab2
--
-- Purp: Test the implementation of Lab2 control unit. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNIMACRO;
use UNIMACRO.vcomponents.all;
use work.lab2Parts.all;

entity lab2_fsm_tb is
end lab2_fsm_tb;

architecture Behavioral of lab2_fsm_tb is

    signal clk, reset_n : STD_LOGIC := '0';
    signal sw, cw : STD_LOGIC_VECTOR(2 downto 0) := (others => '0');
    type state_type is( WaitTrigger, WaitReady, WriteBRAM, BRAMFull);
    signal state: state_type := WaitTrigger;
    
    -- Clock period definitions
    constant clk_period : time := 10 ns;  -- Sets clock to ~ 100MHz

begin

uut : lab2_fsm port map( 
    clk => clk,
    reset_n => reset_n,
    sw => sw,
    cw => cw);
    
   -- Clock process definitions
    clk_process :process
    begin
         clk <= '0';
         wait for clk_period/2;
         clk <= '1';
         wait for clk_period/2;
    end process;
    
    reset_n <= '1' after clk_period*1;
    sw<= "100" after clk_period*3, "000" after clk_period*5, "010" after clk_period*7, "000" after clk_period*9, "010" after clk_period*11, "000" after clk_period*13, "011" after clk_period*15;
    

end Behavioral;