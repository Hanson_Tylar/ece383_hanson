--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 5 Jan 17
-- Course:  ECE 383
-- File: HW1.vhd
-- HW:  HW1
--
-- Purp: Part 6 of HW 1. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity HW1 is
    Port ( I3 : in STD_LOGIC;
           I2 : in STD_LOGIC;
           I1 : in STD_LOGIC;
           I0 : in STD_LOGIC;
           O1 : out STD_LOGIC;
           O0 : out STD_LOGIC);
end HW1;

architecture structure of HW1 is

begin
    O1 <= I3 or I2;
    O0 <= I3 or ((not I2) and I1); 

end structure;
