--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 10 Jan 17
-- Course:  ECE 383
-- File: HW3.vhd
-- HW:  HW3
--
-- Purp: Test functionality of entity HW3.
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity HW3_tb is
end HW3_tb;

architecture behavior of HW3_tb is
    component HW3 is
    Port ( switch_array : in unsigned(7 downto 0);
               LED : out STD_LOGIC);
    end component;
    
    signal switch_array_sig: unsigned(7 downto 0);
    signal LED_sig: STD_LOGIC;
    
    CONSTANT TEST_ELEMENTS:integer:=20;
    SUBTYPE INPUT is unsigned(7 downto 0);
    TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
    SIGNAL TEST_IN: TEST_INPUT_VECTOR := (x"11",x"22",x"33",x"44",x"55",x"66",x"77",x"88",x"99",x"AA",x"BB",x"CC",x"DD",x"EE",x"FF",x"01",x"02",x"03",x"04",x"05");
    
    SUBTYPE OUTPUT is std_logic;
    TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of OUTPUT;
    SIGNAL TEST_OUT: TEST_OUTPUT_VECTOR := ( '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','0','0','0','0' );

    SIGNAL i : integer;
begin
    
        ----------------------------------------------------------------------
        -- Create an instance of your majority
        ----------------------------------------------------------------------
        UUT:    HW3 port map (switch_array_sig, LED_sig);
    
        tb : PROCESS
        BEGIN
        for i in 1 to TEST_ELEMENTS loop
            -----------------------------------------
            -- Parse out the bits of the test_vector
            -----------------------------------------
            switch_array_sig <= test_in(i);
            
            wait for 10 ns; 
            assert LED_sig = test_out(i)
                report "Error with input " & integer'image(i) & " in HW3 circuit "
                severity note;
                    
        end loop;
        
        ---------------------------
        -- Just halt the simulator
        ---------------------------
        assert TRUE = FALSE 
            report "---------------Self-checking testbench completed.  Nominal circuit behavior---------------"
            severity failure;
                
        END PROCESS tb;
    
    end architecture behavior;