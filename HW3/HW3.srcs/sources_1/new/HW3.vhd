--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 10 Jan 17
-- Course:  ECE 383
-- File: HW3.vhd
-- HW:  HW3
--
-- Purp: Turn on an LED when input is a multiple of 17. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity HW3 is
    Port ( switch_array : in unsigned(7 downto 0);
           LED : out STD_LOGIC);
end HW3;

architecture Behavioral of HW3 is
begin
    LED <=  '1' when switch_array = x"11" else
            '1' when switch_array = x"22" else
            '1' when switch_array = x"33" else
            '1' when switch_array = x"44" else
            '1' when switch_array = x"55" else
            '1' when switch_array = x"66" else
            '1' when switch_array = x"77" else
            '1' when switch_array = x"88" else
            '1' when switch_array = x"99" else
            '1' when switch_array = x"AA" else
            '1' when switch_array = x"BB" else
            '1' when switch_array = x"CC" else
            '1' when switch_array = x"DD" else
            '1' when switch_array = x"EE" else
            '1' when switch_array = x"FF" else
            '0';


end Behavioral;
