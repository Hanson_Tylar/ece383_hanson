# Lab 3 - Software Control of a Datapath

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Block Diagram](#hardware-schematic)
3. [Debugging](#debugging)
4. [Results](#results)
5. [Observations and Conclusions](#observations-and-conclusions)
6. [Documentation](#documentation)
 
### Objectives or Purpose 
In this lab, I will integrate the video display controller developed in Lab 2 with the MicroBlaze processor built using the fabric of the Artix-7 FPGA. In the preceding lectures, I learned about the Vivado and SDK tool chains, now its time to put that knowledge to the test by building a software controlled datapath. Lab 2 revealed some shortcomings of my oscilloscope that this lab intends on correcting. Specifically, I will add:

- A horizontal trigger point
- The ability to enable and disable which channels are being displayed
- The ability to trigger off of channel 2
- The ability to change the slope direction of the trigger


### Block Diagram
![](images/BD.jpg)

### Debugging
- In my C code I added a while loop to wait until I received a whole new set of samples, but this caused my ISR to stop functioning. I the while loop I added a useless instruction to act as a NOP so that interrupts will still call myISR.
- I didn't carry lbus and rbus signals out of datapath. This caused nothing to show up on the screen.
- I didn't carry `ch1_enb` and `ch2_enb` signals out of datapath. This didn't allow me to change the enable values.
- I didn't include enable signal in logic for scopeface. I could change the enable values but the scopeface module was not taking this signal into account when drawing pixels.

### Results

Required and A functionality shown in this [video](https://youtu.be/vQJ0ef8V3hg).
B Functionality can be seen in the Lab3.sdk folder of this repository.


### Observations and Conclusions
The objective of this lab was to add software control to lab 2. I added a horizontal trigger point and a user menu for required functionality. I used the ready signal to trigger interrupts and store values for B functionality. Finally, I added options for channel enable, trigger channel, and trigger slope. In this lab I learned a lot about how to use the AXI peripherals and MicroBlaze. I learned how to write to and read from slave registers and even do both of those operations on the same register. I learned in the last lab to use an incremental approach to building complex projects. By following that method I ran into many less errors and headaches that I would have just trying to do it all at once. 

### Documentation
None