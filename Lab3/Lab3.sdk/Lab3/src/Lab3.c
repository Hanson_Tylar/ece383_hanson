/*--------------------------------------------------------------------
-- Name:	C2C Tylar Hanson
-- Date:	March 2, 2017
-- File:	Lab3.c
-- Event:	Lab3
-- Course:		ECE 383
--
-- Purpose:	Implements a custom IP with interrupt to MicroBlaze.
-- 			interfacing lab2 with MicroBlaze
--
-- Documentation:	None
--
-- Academic Integrity Statement: I certify that, while others may have
-- assisted me in brain storming, debugging and validating this program,
-- the program itself is my own work. I understand that submitting code
-- which is the work of other individuals is a violation of the honor
-- code.  I also understand that if I knowingly give my original work to
-- another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
/***************************** Include Files ********************************/
#include <stdio.h>
#include <xuartlite_l.h>
#include <xil_io.h>
#include <xil_exception.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xstatus.h"
/************************** Constant Definitions ****************************/
/* Define Constants for Oscilloscope PCORE */
#define	uartRegAddr 			0x40600000
#define oscopeBase				0x44a00000
#define oscopeLbusReg			oscopeBase
#define oscopeRbusReg			oscopeBase + 4
#define oscopeflagQReg			oscopeBase + 8
#define oscopeflagClearReg		oscopeBase + 8
#define oscopeexWrAddrReg		oscopeBase + 12
#define oscopeexWenReg			oscopeBase + 16
#define oscopeexSelReg			oscopeBase + 20
#define oscopeexLbusReg			oscopeBase + 24
#define oscopeexRbusReg			oscopeBase + 28
#define oscopetriggerVoltReg	oscopeBase + 32
#define oscopetriggerTimeReg	oscopeBase + 36
#define oscopeCh1EnbReg			oscopeBase + 40
#define oscopeCh2EnbReg			oscopeBase + 44

#define printf xil_printf

/* Define Constants for flagRegister control */
#define readyBit 0x04

/* Global Variables */
u16 triggerVolt = 220;
u16 triggerTime = 320;
u16 LBus_array[sizeof(u16)*1024];
u16 RBus_array[sizeof(u16)*1024];
int getNewSample = 0;
int array_index = 0;
int array_full = 0;


void myISR(){
    microblaze_disable_interrupts();
	if(getNewSample == 1 && array_full == 0){
		if((Xil_In8(oscopeflagQReg) & readyBit) == readyBit){
			LBus_array[array_index] =  Xil_In16(oscopeLbusReg);
			RBus_array[array_index] =  Xil_In16(oscopeRbusReg);
			array_index++;
			if(array_index > 1023){
				array_full = 1;
				getNewSample = 0;
			}
			Xil_Out8(oscopeflagClearReg, readyBit);
		}
		Xil_Out8(oscopeflagClearReg, 0x00);
	}
    microblaze_enable_interrupts();
	return;
}

int main()
{
	u16 previous = 0;
	u16 next = 0;
	int triggerEvent = 0;
	int triggerChannel = 0;
	int posSlope = 1;
	unsigned char c;
    init_platform();
    printf("Welcome to Lab3\n\r");

    microblaze_register_handler((XInterruptHandler) myISR, (void *) 0);
    microblaze_enable_interrupts();
    Xil_Out16(oscopeflagClearReg, readyBit);
    Xil_Out16(oscopeflagClearReg, 0x00);
    Xil_Out16(oscopeexSelReg, 0x01);
    Xil_Out16(oscopeexWenReg, 0x00);
    Xil_Out16(oscopetriggerVoltReg, triggerVolt);
    Xil_Out16(oscopetriggerTimeReg, triggerTime);

    while(1){
    	c=XUartLite_RecvByte(uartRegAddr);
    	switch(c){
			case '?':
				printf("--------------------------\r\n");
				printf("  Trigger Voltage = %d\r\n", Xil_In16(oscopetriggerVoltReg));
				printf("   Trigger Time = %d\r\n", Xil_In16(oscopetriggerTimeReg));
				printf("    L Bus Out = %d\r\n", Xil_In16(oscopeLbusReg));
				printf("    R Bus Out = %d\r\n", Xil_In16(oscopeRbusReg));
				printf("--------------------------\r\n");
				printf("?: Help menu\r\n");
				printf("w: Decrease trigger voltage\r\n");
				printf("s: Increase trigger voltage\r\n");
				printf("a: Increase trigger time\r\n");
				printf("d: Decrease trigger time\r\n");
				printf("1: Enable/Disable Channel 1\r\n");
				printf("2: Enable/Disable Channel 2\r\n");
				printf("t: Trigger on Ch1/Ch2\r\n");
				printf("m: Change slope of trigger\r\n");
				printf("--------------------------\r\n");
				break;
			case 'w':
				if(triggerVolt >=30){ triggerVolt -= 10; }
				Xil_Out16(oscopetriggerVoltReg, triggerVolt);
				break;
			case 's':
				if(triggerVolt <= 410){ triggerVolt += 10; }
				Xil_Out16(oscopetriggerVoltReg, triggerVolt);
				break;
			case 'a':
				if(triggerTime >= 35){ triggerTime -= 15; }
				Xil_Out16(oscopetriggerTimeReg, triggerTime);
				break;
			case 'd':
				if(triggerTime <= 605){ triggerTime += 15; }
				Xil_Out16(oscopetriggerTimeReg, triggerTime);
				break;
			case '1':
				if(Xil_In16(oscopeCh1EnbReg) == 1){
					Xil_Out16(oscopeCh1EnbReg, 0x00);
				}
				else{
					Xil_Out16(oscopeCh1EnbReg, 0x01);
				}
				break;
			case '2':
				if(Xil_In16(oscopeCh2EnbReg) == 1){
					Xil_Out16(oscopeCh2EnbReg, 0x00);
				}
				else{
					Xil_Out16(oscopeCh2EnbReg, 0x01);
				}
				break;
			case 't':
				if(triggerChannel == 0){
					triggerChannel = 1;
				}
				else{
					triggerChannel = 0;
				}
				break;
			case 'm':
				if(posSlope == 1){
					posSlope = 0;
				}
				else{
					posSlope = 1;
				}
				break;
			default:
				printf("unrecognized character: %c\r\n",c);
				break;
			}
    	// Wait for a full array of new data after a button press
    	getNewSample = 1;
    	array_full = 0;
    	array_index = 0;
    	// Wait until full, previous = 0 used as a NOP to allow interrupts to occur.
    	while(array_full != 1){
    		previous = 0;
    	}

    	// find a trigger event
		previous = 0;
		next = 0;
		triggerEvent = 0;
		array_index = 1;
		while(triggerEvent == 0 && array_index < 1023){
			// What channel to trigger on
			if(triggerChannel == 0){
				previous = ((LBus_array[array_index-1])>>6)-803;
				next = ((LBus_array[array_index])>>6)-803;
			}
			else{
				previous = ((RBus_array[array_index-1])>>6)-803;
				next = ((RBus_array[array_index])>>6)-803;
			}
			// What is the direction of the slope
			if(posSlope == 1){
				if(previous >= triggerVolt && next < triggerVolt && array_index >= triggerTime){
					triggerEvent = 1;
				}
				else{
					triggerEvent = 0;
					array_index++;
				}
			}
			else{
				if(previous < triggerVolt && next >= triggerVolt && array_index >= triggerTime){
					triggerEvent = 1;
				}
				else{
					triggerEvent = 0;
					array_index++;
				}
			}
		} // end while

		// Write samples from left edge to time trigger
		if(triggerEvent == 1){
			Xil_Out16(oscopeexWenReg, 0x01);
			for(int j = 20; j < array_index; j++){
				Xil_Out16(oscopeexWrAddrReg, j);
				Xil_Out16(oscopeexLbusReg, LBus_array[array_index-triggerTime+j]);
				Xil_Out16(oscopeexRbusReg, RBus_array[array_index-triggerTime+j]);
			}
			// Write samples from time trigger to right edge of screen
			int k = 0;
			while(array_index < 1024){
				Xil_Out16(oscopeexWrAddrReg, triggerTime+k);
				Xil_Out16(oscopeexLbusReg, LBus_array[array_index]);
				Xil_Out16(oscopeexRbusReg, RBus_array[array_index]);
				array_index++;
				k++;
			}
			Xil_Out16(oscopeexWenReg, 0x00);
		}
	} // end while(1)

    cleanup_platform();
    return 0;
}
