----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2017 01:47:35 PM
-- Design Name: 
-- Module Name: FSM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM is
    Port ( 
        clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        sw : in STD_LOGIC_VECTOR (1 downto 0);
        cw : out STD_LOGIC_VECTOR (3 downto 0));
end FSM;

architecture Behavioral of FSM is
    type state_type is(floor1, floor2, floor3, floor4);
    signal state: state_type := floor1;
    signal up_down, stop : STD_LOGIC;
    
begin
    up_down <= sw(1);
    stop <= sw(0);
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                state <= floor1;
            else
                case state is
                    when floor1 =>
                        if(stop = '0' and up_down = '1') then
                            state <= floor2;
                        end if;
                    when floor2 =>
                        if(stop = '0' and up_down = '1') then
                            state <= floor3;
                        elsif(stop = '0' and up_down = '0') then
                            state <= floor1;
                        end if;
                    when floor3 =>
                        if(stop = '0' and up_down = '1') then
                            state <= floor4;
                        elsif(stop = '0' and up_down = '0') then
                            state <= floor2;
                        end if;
                    when floor4 =>
                        if(stop = '0' and up_down = '0') then
                            state <= floor3;
                        end if;                    
                end case;
            end if;
        end if;
    end process;
    
    cw <= "0001" when state = floor1 else
        "0010" when state = floor2 else
        "0100" when state = floor3 else
        "1000" when state = floor4 else
        "0000";


end Behavioral;
