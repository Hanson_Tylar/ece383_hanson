----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2017 01:45:58 PM
-- Design Name: 
-- Module Name: FSMQuiz - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSMQuiz is
    Port ( clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           switch : in STD_LOGIC_VECTOR (1 downto 0);
           LED : out STD_LOGIC_VECTOR (3 downto 0));
end FSMQuiz;

architecture Behavioral of FSMQuiz is

signal FSM_Clk : STD_LOGIC_VECTOR(26 downto 0);

component Clock_Divider is
	 port(
		 clk : in STD_LOGIC;
		 clockbus : out STD_LOGIC_VECTOR(26 downto 0)
	     );
end component;

component FSM is
    Port ( 
        clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        sw : in STD_LOGIC_VECTOR (1 downto 0);
        cw : out STD_LOGIC_VECTOR (3 downto 0));
end component;

begin

myClockDivider : Clock_Divider
port map(
    clk => clk,
    clockbus => FSM_Clk);
    
myFSM : FSM
port map(
    clk => FSM_Clk(26),
    reset_n => reset_n,
    sw => switch,
    cw => LED);

end Behavioral;
