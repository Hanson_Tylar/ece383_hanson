----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/23/2017 03:26:36 PM
-- Design Name: 
-- Module Name: lab1_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity lab1_top_tb is
end lab1_top_tb;

architecture Behavioral of lab1_top_tb is
    component lab1
    port ( clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           btn: in    STD_LOGIC_VECTOR(4 downto 0);
           tmds : out  STD_LOGIC_VECTOR (3 downto 0);
           tmdsb : out  STD_LOGIC_VECTOR (3 downto 0));
   end component; 
      
  
     --Inputs
     signal clk : std_logic := '0';
     signal reset_n : std_logic := '0';
     signal btn : STD_LOGIC_VECTOR(4 downto 0):= (others => '0');
     --Outputs
     signal tmds : STD_LOGIC_VECTOR (3 downto 0);
     signal tmdsb : STD_LOGIC_VECTOR (3 downto 0);
     -- Other
     signal button_activity : STD_LOGIC_VECTOR (4 downto 0);
  
     -- Clock period definitions
     constant clk_period : time := 10 ns;
   
  BEGIN
   
      -- Instantiate the Unit Under Test (UUT)
     uut: lab1 PORT MAP (
           clk => clk,
           reset_n => reset_n,
           btn => btn,
           tmds => tmds,
           tmdsb => tmdsb
          );
  
     -- Clock process definitions
     clk_process :process
     begin
          clk <= '0';
          wait for clk_period/2;
          clk <= '1';
          wait for clk_period/2;
     end process;
   
      reset_n <= '0', '1' after 10nS;
      btn <= "00001" after 1us, "00000" after 2 us;
      button_activity <= "00000", "00001" after 1us, "00000" after 2us;
end Behavioral;
