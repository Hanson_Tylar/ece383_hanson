--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 18 Jan 17
-- Course:  ECE 383
-- File: scopeFace.vhd
-- HW:  Lab1
--
-- Purp: This entity only contains combinational logic. When given a 
-- row,column pair, its responsible for generating the R,G,B value 
-- of that pixel.
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity scopeFace is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
		   trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
		   ch1: in std_logic;
		   ch1_enb: in std_logic;
		   ch2: in std_logic;
		   ch2_enb: in std_logic);
end scopeFace;

architecture Behavioral of scopeFace is

signal row_grid : STD_LOGIC := '0';
signal col_grid : STD_LOGIC := '0';
signal tick_grid_h: STD_LOGIC := '0';
signal tick_grid_v: STD_LOGIC := '0';
signal valid_tick_row: STD_LOGIC := '0';
signal valid_tick_col: STD_LOGIC := '0';
signal tr_time_mark : STD_LOGIC := '0';
signal tr_volt_mark : STD_LOGIC := '0';
signal tr_volt_line : STD_LOGIC := '0';

begin

    row_grid <= '1' when row = 20 and column >=20 and column <= 620 else
                '1' when row = 70 and column >=20 and column <= 620 else
                '1' when row = 120 and column >=20 and column <= 620 else
                '1' when row = 170 and column >=20 and column <= 620 else
                '1' when row = 220 and column >=20 and column <= 620 else
                '1' when row = 270 and column >=20 and column <= 620 else
                '1' when row = 320 and column >=20 and column <= 620 else
                '1' when row = 370 and column >=20 and column <= 620 else
                '1' when row = 420 and column >=20 and column <= 620 else
                '0';
    
    col_grid <= '1' when column = 20 and row >=20 and row <= 420 else
                '1' when column = 80 and row >=20 and row <= 420  else
                '1' when column = 140 and row >=20 and row <= 420  else
                '1' when column = 200 and row >=20 and row <= 420  else
                '1' when column = 260 and row >=20 and row <= 420  else
                '1' when column = 320 and row >=20 and row <= 420  else
                '1' when column = 380 and row >=20 and row <= 420  else
                '1' when column = 440 and row >=20 and row <= 420  else
                '1' when column = 500 and row >=20 and row <= 420  else
                '1' when column = 560 and row >=20 and row <= 420  else
                '1' when column = 620 and row >=20 and row <= 420  else
                '0';
                
    valid_tick_row <= '1' when row = 30 or row = 40 or row = 50 or row = 60 or row = 80 or row = 90 or row = 100 or row = 110 or row = 130 or row = 140 or row = 150 or row = 160 or row = 180 or row = 190 or row = 200 or row = 210 or row = 230 or row = 240 or row = 250 or row = 260 or row = 280 or row = 290 or row = 300 or row = 310 or row = 330 or row = 340 or row = 350 or row = 360 or row = 380 or row = 390 or row = 400 or row = 410 else
                      '0';
                      
    valid_tick_col <= '1' when column = 35 or column = 50 or column = 65 or column = 95 or column = 110 or column = 125 or column = 155 or column = 170 or column = 185 or column = 215 or column = 230 or column = 245 or column = 275 or column = 290 or column = 305 or column = 335 or column = 350 or column = 365 or column = 395 or column = 410 or column = 425 or column = 455 or column = 470 or column = 485 or column = 515 or column = 530 or column = 545 or column = 575 or column = 590 or column = 605 else
                      '0';
                      
    tick_grid_h <= '1' when column >= 318 and column <= 322 and valid_tick_row = '1' else '0';
    
    tick_grid_v <= '1' when row >= 218 and row <= 222 and valid_tick_col = '1' else '0';               
             
    tr_time_mark <= '1' when (column >= trigger_time - 2) and (column <= trigger_time + 2) and row = 21 else
                        '1' when (column >= trigger_time -1) and (column <= trigger_time + 1) and row = 22 else
                        '1' when (column = trigger_time) and row = 23 else
                        '0';
                        
    tr_volt_mark <= '1' when (row >= trigger_volt - 2) and (row <= trigger_volt + 2) and column = 21 else
                    '1' when (row >= trigger_volt - 1) and (row <= trigger_volt + 1) and column = 22 else
                    '1' when (row = trigger_volt) and column = 23 else
                    '0';
                    
   tr_volt_line <= '1' when (row = trigger_volt) and column > 23 and column <= 620 else '0';
    
    r <= x"FF" when ch1 = '1' else
         x"00" when ch2 = '1' else
         x"FF" when tr_volt_line = '1' else
         x"FF" when row_grid = '1' or col_grid = '1' or tick_grid_h = '1' or tick_grid_v = '1' or tr_time_mark = '1' or tr_volt_mark = '1' else
         x"00";
         
    g <= x"FF" when ch1 = '1' or ch2 = '1' else
         x"A5" when tr_volt_line = '1' else
         x"FF" when row_grid = '1' or col_grid = '1' or tick_grid_h = '1' or tick_grid_v = '1' or tr_time_mark = '1' or tr_volt_mark = '1' else
         x"00";
         
    b <= x"00" when ch1 = '1' or ch2 = '1' else
         x"00" when tr_volt_line = '1' or tr_time_mark = '1' or tr_volt_mark = '1' else
         x"FF" when row_grid = '1' or col_grid = '1' or tick_grid_h = '1' or tick_grid_v = '1' else  
         x"00";

end Behavioral;
