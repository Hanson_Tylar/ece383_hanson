--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 18 Jan 17
-- Course:  ECE 383
-- File: vga.vhd
-- HW:  Lab1
--
-- Purp: This component sweeps across the display from left to 
-- right, and then return to the left side of the next lower row. 
-- The VGA interface determines the color of each pixel on this 
-- journey with the help of the scopeFace component. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vga is
	Port(	clk: in  STD_LOGIC;
			reset_n : in  STD_LOGIC;
			h_sync : out  STD_LOGIC;
			v_sync : out  STD_LOGIC; 
			blank : out  STD_LOGIC;
			r: out STD_LOGIC_VECTOR(7 downto 0);
			g: out STD_LOGIC_VECTOR(7 downto 0);
			b: out STD_LOGIC_VECTOR(7 downto 0);
			trigger_time: in unsigned(9 downto 0);
			trigger_volt: in unsigned (9 downto 0);
			row: out unsigned(9 downto 0);
			column: out unsigned(9 downto 0);
			ch1: in std_logic;
			ch1_enb: in std_logic;
			ch2: in std_logic;
			ch2_enb: in std_logic);
end vga;

architecture Behavioral of vga is

component column_counter is
    Port ( clk, reset_n, ctrl : in STD_LOGIC;
            roll : out STD_LOGIC;
            column : out unsigned(9 downto 0));
end component;

component row_counter is
    Port ( clk, reset_n, ctrl : in STD_LOGIC;
            roll : out STD_LOGIC;
            row : out unsigned(9 downto 0));
end component;

component scopeFace is
    Port ( row : in  unsigned(9 downto 0);
           column : in  unsigned(9 downto 0);
		   trigger_volt: in unsigned (9 downto 0);
		   trigger_time: in unsigned (9 downto 0);
           r : out  std_logic_vector(7 downto 0);
           g : out  std_logic_vector(7 downto 0);
           b : out  std_logic_vector(7 downto 0);
		   ch1: in std_logic;
		   ch1_enb: in std_logic;
		   ch2: in std_logic;
		   ch2_enb: in std_logic);
end component;

signal roll_sig : STD_LOGIC := '0';
signal c_sig: unsigned(9 downto 0):= (others => '0');
signal r_sig: unsigned(9 downto 0):= (others => '0');
signal h_blank: STD_LOGIC := '0';
signal v_blank: STD_LOGIC := '0';

begin

    c_counter: column_counter
    port map( clk => clk, reset_n => reset_n, ctrl => '1', roll => roll_sig, column => c_sig ); 
    r_counter: row_counter
    port map( clk => clk, reset_n => reset_n, ctrl => roll_sig, roll => OPEN, row => r_sig );
    scopeFace0: scopeFace
    port map ( row => r_sig, column => c_sig, trigger_volt => trigger_volt, trigger_time => trigger_time, r => r, g => g, b => b, ch1 => ch1, ch1_enb => ch1_enb, ch2 => ch2, ch2_enb => ch2_enb );

    h_sync <= '1' when ((c_sig >= 0 and c_sig < 656) or (c_sig >= 752 and c_sig < 800)) else '0';
    h_blank <= '0' when (c_sig >= 0 and c_sig < 640) else '1';
    
    v_sync <= '1' when ((r_sig >= 0 and r_sig < 490) or (r_sig >= 492 and r_sig < 525)) else '0';
    v_blank <= '0' when (r_sig >= 0 and r_sig < 480) else '1';
    
    blank <= h_blank or v_blank;
    
    row <= r_sig;
    column <= c_sig;
    
end Behavioral;
