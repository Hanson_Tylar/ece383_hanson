--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 18 Jan 17
-- Course:  ECE 383
-- File: column_counter.vhd
-- HW:  Lab1
--
-- Purp: This increments the value of the column and row locations
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity column_counter is
    Port ( clk, reset_n, ctrl : in STD_LOGIC;
       roll : out STD_LOGIC;
       column : out unsigned(9 downto 0));
end column_counter;

architecture Behavioral of column_counter is
    signal col_sig : unsigned(9 downto 0);
    
begin
    process(clk)
    begin
        if (rising_edge(clk)) then 
            if (reset_n = '0') then
                col_sig <= (others => '0');
            elsif ((ctrl = '1') and (col_sig < 799)) then
                col_sig <= col_sig + 1;
            elsif ((ctrl = '1') and (col_sig = 799)) then
                col_sig <= (others => '0');
            end if;
        end if;
    end process;
    
    roll <= '1' when (col_sig = 799 and ctrl = '1') else '0';
    column <= col_sig;

end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity row_counter is
    Port ( clk, reset_n, ctrl : in STD_LOGIC;
       roll : out STD_LOGIC;
       row : out unsigned(9 downto 0));
end row_counter;

architecture Behavioral of row_counter is
    signal row_sig : unsigned(9 downto 0);
    
begin
    process(clk)
    begin
        if (rising_edge(clk)) then 
            if (reset_n = '0') then
                row_sig <= (others => '0');
            elsif ((ctrl = '1') and (row_sig < 524)) then
                row_sig <= row_sig + 1;
            elsif ((ctrl = '1') and (row_sig = 524)) then
                row_sig <= (others => '0');
            end if;
        end if;
    end process;
    
    row <= row_sig;

end Behavioral;