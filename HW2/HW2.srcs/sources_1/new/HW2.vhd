--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 6 Jan 17
-- Course:  ECE 383
-- File: HW2.vhd
-- HW:  HW2
--
-- Purp: Scancode Decoder. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Scancode_Decoder is
    Port ( D : in STD_LOGIC_VECTOR (7 downto 0);
           H : out STD_LOGIC_VECTOR (3 downto 0));
end Scancode_Decoder;

architecture Behavioral of Scancode_Decoder is

begin

    H <=    x"0" when D = x"45" else
            x"1" when D = x"16" else
            x"2" when D = x"1E" else
            x"3" when D = x"26" else
            x"4" when D = x"25" else
            x"5" when D = x"2E" else
            x"6" when D = x"36" else
            x"7" when D = x"3D" else
            x"8" when D = x"3E" else
            x"9" when D = x"46" else
            x"0";
    

end Behavioral;
