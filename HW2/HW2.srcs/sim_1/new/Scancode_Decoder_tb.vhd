--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 6 Jan 17
-- Course:  ECE 383
-- File: Scancode_Decoder_tb.vhd
-- HW:  HW2
--
-- Purp: Scancode Decoder. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity Scancode_Decoder_tb is 
end entity Scancode_Decoder_tb;

architecture behavior of Scancode_Decoder_tb is

	----------------------------------------------------------------------
	-- These signal names must match the names of the I/O markers
	----------------------------------------------------------------------
	component Scancode_Decoder is
        Port ( D : in STD_LOGIC_VECTOR (7 downto 0);
               H : out STD_LOGIC_VECTOR (3 downto 0));
	end component;
  
  
	signal D: STD_LOGIC_VECTOR(7 downto 0);
	signal H: STD_LOGIC_VECTOR(3 downto 0);
	  
	CONSTANT TEST_ELEMENTS:integer:=10;
	SUBTYPE INPUT is std_logic_vector(7 downto 0);
	TYPE TEST_INPUT_VECTOR is array (1 to TEST_ELEMENTS) of INPUT;
	SIGNAL TEST_IN: TEST_INPUT_VECTOR := (	x"45", x"16", x"1E", x"26", x"25", x"2E", x"36", x"3D", x"3E", x"46" );
    
    SUBTYPE OUTPUT is std_logic_vector(3 downto 0);
	TYPE TEST_OUTPUT_VECTOR is array (1 to TEST_ELEMENTS) of OUTPUT;
	SIGNAL TEST_OUT: TEST_OUTPUT_VECTOR := ( x"0", x"1", x"2", x"3", x"4", x"5", x"6", x"7", x"8", x"9" );

	SIGNAL i : integer;		

begin

	----------------------------------------------------------------------
	-- Create an instance of your majority
	----------------------------------------------------------------------
	UUT:	Scancode_Decoder port map (D, H);

	tb : PROCESS
	BEGIN
	for i in 1 to TEST_ELEMENTS loop
		-----------------------------------------
		-- Parse out the bits of the test_vector
		-----------------------------------------
		D <= test_in(i);
		
		wait for 10 ns; 
		assert H = test_out(i)
 				report "Error with input " & integer'image(i) & " in Scancode Decoder circuit "
				severity failure;
				
	end loop;
	
	---------------------------
	-- Just halt the simulator
	---------------------------
	assert TRUE = FALSE 
		report "---------------Self-checking testbench completed.  Nominal circuit behavior---------------"
		severity failure;
			
	END PROCESS tb;

end architecture behavior;