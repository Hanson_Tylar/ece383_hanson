--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 2 Feb 17
-- Course:  ECE 383
-- File: SCR_cu.vhd
-- HW:  HW8
--
-- Purp: Control unit entity for a scancode reader
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SCR_cu is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           kbClk : in STD_LOGIC;
           sw : in STD_LOGIC;
           cw : out STD_LOGIC_VECTOR(2 downto 0);
           busy : out STD_LOGIC);
end SCR_cu;

architecture Behavioral of SCR_cu is
    type state_type is (Init, StartReceive, WhileClkHigh, ShiftData, WhileClkLow);
        signal state: state_type := Init;
begin
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset= '0') then
                state <= Init;
            else
                case state is
                    when Init =>
                        busy <= '0';
                        cw <= "011";
                        if(kbClk = '0') then state <= StartReceive; end if;
                    when StartReceive =>
                        busy <= '1';
                        cw <= "000";
                        state <= WhileClkHigh;
                    when WhileClkHigh =>
                        cw <= "000";
                        if(kbClk = '0' and sw = '0') then state <= ShiftData;
                        elsif(sw = '1') then state <= Init; end if;
                    when ShiftData =>
                        cw <= "101";
                        state <= WhileClkLow;
                    when WhileClkLow =>
                        cw <= "000";
                        if(kbClk = '1') then state <= WhileClkHigh; end if;
                end case;
            end if;
        end if;
    end process;

end Behavioral;
