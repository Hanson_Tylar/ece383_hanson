# Lab 4 - Function Generation

## By C2C Tylar Hanson

## Table of Contents
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Debugging](#debugging)
3. [Milestone 1](#milestone-1)
4. [Milestone 2](#milestone-2)
5. [Results](#results)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Documentation](#documentation)
 
### Objectives or Purpose 
You are to use Direct Digital Synthesis to reproduce your audio waveform. You may choose any waveform so long as its not Piecewise Linear. A few interesting examples would be sinusoids, the sinc function, exponentially damped sinusoids, or a waveform from a musical instrument (guitar, piano, or clarinet). 

##### Requirements
While you have the flexibility to design the waveform generator as you see fit, your system must meet the following requirements:

- Use an update rate of 48kHz
- At 440Hz, the LUT should be incremented by about 1 index.
- Be able to make between a 1Hz and 0.25Hz change in frequency.
- Be able to generate a full amplitude waveform.
	- Bonus: Connect your Function Generator hardware to your Lab 3 functionality to enable the display of your generated signals on your Lab 3 O'scope display.
	- 
### Debugging
I had some issues when first generating a bit stream because I was including ports needed for HMDI functions in my entities and in the constraints file. After removing those I was able to generate a bit stream. I had an issue with my delta being the result of subtracting the wrong numbers. To fix this I added an extra clock cycle in between each state to allow time for BRAM to output new values and for my base and next values to update as well. When generating a waveform I had what looked like a periodic glitch at the peak of the signal. I figured out this was due to the base plus offset*delta value overflowing. I added logic to check for this and everything was good from then on.

### Milestone 1

![](images/Lab4BlockDiagram.jpg)

###### RDADDR_process
This process controls the read address values for the left and right BRAMs. The control words tell when to increase or decrease the amplitude or frequency of the waveform. When decreasing the frequency the phase increment is decreased by the amount set on the slide switches and when increasing the frequency the phase increment equal phase increment plus the amount on the slide switches.

###### Interpolation_proc
This process interpolates values for the left and right buses. It will take in two samples and calculate the difference, delta. It also takes in the offset of the index. The process will then add the offset times delta to the base value. This will be the output of the process. 

### Milestone 2
The calculations for the BRAM values can be seen in the BRAM.xlsx file.
![](images/Milestone2.JPG)

### Results
This waveform shows the transition between states for one sample.

![](images/states.JPG)

This waveform shows several new values being sent to the audio codec on the ready signal.

![](images/samples.JPG)

This video shows Lab 4 functionality on the oscilloscope built in Lab 3.
[https://youtu.be/aYSgU3cII7Q](https://youtu.be/aYSgU3cII7Q)

### Observations and Conclusions
The purpose of this lab was to generate a waveform by using direct digital synthesis. This was accomplished. Interpolation was even used to create a smoother output. I learned many things doing this lab. I learned how changing how much you increment a BRAM index affects the frequency of the resulting waveform. I learned a bit about the fixed point math between unsigned and signed numbers, but still don't fully understand it. Things I will take away from this lab is writing less code at a time and testing more often as well as getting a good test bench from the beginning. 

### Documentation
None