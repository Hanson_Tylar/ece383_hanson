library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.lab4parts.all;

entity Lab4_tb is
end Lab4_tb;

architecture Behavioral of Lab4_tb is

    signal clk : STD_LOGIC;
    signal reset_n : STD_LOGIC;
    signal ac_mclk : STD_LOGIC;
    signal ac_adc_sdata : STD_LOGIC;
    signal ac_dac_sdata : STD_LOGIC;
    signal ac_bclk : STD_LOGIC;
    signal ac_lrclk : STD_LOGIC;
    signal scl : STD_LOGIC;
    signal sda : STD_LOGIC;
    signal btn : STD_LOGIC_VECTOR(4 downto 0);
    signal ssw : STD_LOGIC_VECTOR(7 downto 0);
    signal sw : STD_LOGIC := '0';
    signal cw : STD_LOGIC_VECTOR(6 downto 0);
    constant clk_period : time := 10 ns;
    
begin

   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
   
   ready_proc: process
   begin
        sw <='0';
        wait for 20.83 us;
        sw <='1';
        wait for clk_period;
   end process;
   
    UUT_datapath : Lab4_dp port map(
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        scl => scl,
        sda => sda,
        ssw => ssw,
        sw => OPEN,
        cw => cw
    );
    
    UUT_controlUnit : Lab4_cu port map(
        clk => clk,
        reset_n => reset_n,
        btn => btn,
        sw => sw,
        cw => cw
    );
    
    reset_n <= '0', '1' after clk_period;
--    sw <= '1' after clk_period*5, '0' after clk_period*7, '1' after clk_period*12, '0' after clk_period*14;
--    btn <= "00000", "01000" after clk_period*15, "00000" after clk_period*17;
--    ssw <= "00000001";
end Behavioral;
