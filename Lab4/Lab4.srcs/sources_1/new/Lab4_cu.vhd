--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 7 Mar 17
-- Course:  ECE 383
-- File: Lab4_cu.vhd
-- HW:  Lab4
--
-- Purp: Controls the datapath. Says when to read values from BRAM 
--       and handles button debouncing.
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Lab4_cu is
    Port ( clk : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    btn : in STD_LOGIC_VECTOR(4 downto 0);
    sw : in STD_LOGIC;
    cw : out STD_LOGIC_VECTOR(6 downto 0));
end Lab4_cu;

architecture Behavioral of Lab4_cu is

    type state_type is( reset, waitActivity, incFreq, decFreq, incAmp, decAmp, getBaseValue, getNextValue, swChannel, waitClkCycle1, waitClkCycle2 );
    signal state : state_type := reset;
    signal old_button, button_activity : STD_LOGIC_VECTOR(4 downto 0) := (others => '0');
            
begin

    process(clk)
    begin
        if(rising_edge(clk)) then
            if reset_n = '0' then
                button_activity <= "00000";
                old_button <= "00000";
            else
                button_activity <= (btn xor old_button) and btn;
                old_button <= btn;
            end if;
        end if;
    end process;
    
    process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                state <= reset;
            else
                case state is
                    when reset =>
                        state <= waitActivity;
                    when waitActivity =>
                        if(sw = '1') then
                            state <= getBaseValue;
                        elsif(button_activity(3) = '1') then
                            state <= incFreq;
                        elsif(button_activity(1) = '1') then
                            state <= decFreq;
                        elsif(button_activity(0) = '1') then
                            state <= incAmp;
                        elsif(button_activity(2) = '1') then
                            state <= decAmp;
                        elsif(button_activity(4) = '1') then
                            state <= swChannel;
                        end if;
                    when incFreq =>
                        state <= waitActivity;
                    when decFreq =>
                        state <= waitActivity;
                    when incAmp =>
                        state <= waitActivity;
                    when decAmp =>
                        state <= waitActivity;                    
                    when getBaseValue =>
                        state <= waitClkCycle1;
                    when waitClkCycle1 =>
                        state <= getNextValue;
                    when getNextValue =>
                        state <= waitClkCycle2;
                    when waitClkCycle2 =>
                        state <= waitActivity;
                    when swChannel =>
                        state <= waitActivity;
                end case;
            end if;
        end if;
    end process;
    cw <= "1000000" when state = incFreq else
          "0100000" when state = decFreq else
          "0010000" when state = incAmp else
          "0001000" when state = decAmp else
          "0000100" when state = getBaseValue else
          "0000010" when state = getNextValue else
          "0000001" when state = waitClkCycle2 else
          "0000000";
end Behavioral;
