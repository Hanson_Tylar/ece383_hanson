--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 7 Mar 17
-- Course:  ECE 383
-- File: Lab4_dp.vhd
-- HW:  Lab4
--
-- Purp: Reads values from BRAM and does the interpolation math needed
--       to generate a smooth periodic waveform.
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.lab4parts.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity Lab4_dp is
    Port ( clk : in  STD_LOGIC;
    reset_n : in  STD_LOGIC;
    ac_mclk : out STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;    
    ssw : in std_logic_vector(7 downto 0);
    sw : out STD_LOGIC;
    cw : in std_logic_vector (6 downto 0));
end Lab4_dp;

architecture behavior of Lab4_dp is

    signal reset : STD_LOGIC := '0';
    signal D_out_L, D_out_R, DI : STD_LOGIC_VECTOR(15 downto 0) := (others => '0');
    signal baseValue, nextValue, delta : unsigned(15 downto 0) := (others => '0');
    signal index, L_bus_in, R_bus_in : STD_LOGIC_VECTOR(17 downto 0):= (others => '0'); 
    signal offset : unsigned(7 downto 0):= (others => '0');
    signal RDADDR : STD_LOGIC_VECTOR(9 downto 0):= (others => '0');
    signal phase_inc : unsigned(17 downto 0) := (others => '0');
    signal offsetXdelta : unsigned(23 downto 0) := (others => '0');
    
begin

    reset <= not reset_n;
    
    btn_proc : process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                phase_inc <= to_unsigned(2400,18);
            elsif(cw(6) = '1') then -- Increase Frequency
                phase_inc <= phase_inc+unsigned("0000000000"&ssw);
            elsif(cw(5) = '1') then
                phase_inc <= phase_inc-unsigned("0000000000"&ssw);
            end if;
        end if;
    end process;
       
    RDADDR_proc : process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                index <= (others => '0');
                RDADDR <= (others => '0');
            elsif(cw(2) = '1') then -- If get base value
                index <= STD_LOGIC_VECTOR(unsigned(index) + phase_inc);
                RDADDR <= STD_LOGIC_VECTOR(unsigned(RDADDR) + 1); 
                offset <= unsigned(index(7 downto 0));
            elsif(cw(1) = '1') then -- If get next value
                RDADDR <= index(17 downto 8);
            end if;
        end if;
    end process;
    
    offsetXdelta <= offset*delta;
    Interpolation_proc : process(clk)
    begin
        if(rising_edge(clk)) then
            if(reset_n = '0') then
                L_bus_in <= (others => '0');
                R_bus_in <= (others => '0');
            elsif(cw(2) = '1') then -- If get base value
                baseValue <= unsigned(D_out_L);
            elsif(cw(1) = '1') then -- If get next value
                nextValue <= unsigned(D_out_L);
            elsif(cw(0) = '1') then
                if nextValue<baseValue then
                    delta <= baseValue-nextValue;
                else
                    delta <= nextValue-baseValue;
                end if;
                if((baseValue+offsetXdelta(23 downto 8))<baseValue) then -- Check for overflow
                    L_bus_in <= STD_LOGIC_VECTOR(baseValue-x"8000")&"00";
                else
                    L_bus_in <= STD_LOGIC_VECTOR((baseValue + offsetXdelta(23 downto 8))-x"8000")&"00";
                end if;
            end if;
        end if;
    end process;
    
    BRAM_L: BRAM_SDP_MACRO 
        generic map (
        BRAM_SIZE => "18Kb",            -- Target BRAM, "18Kb" or "36Kb"
        DEVICE => "7SERIES",            -- Target device: "VIRTEX5", "VIRTEX6", "SPARTAN6", "7SERIES"
        DO_REG => 0,                 -- Optional output register disabled
        INIT => X"000000000000000000",    -- Initial values on output port
        INIT_FILE => "NONE",            -- 
        WRITE_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        READ_WIDTH => 16,            -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
        SIM_COLLISION_CHECK => "NONE",     -- Simulation collision check
        SRVAL => X"000000000000000000",    -- Set/Reset value for port output
--        INIT_00 => X"8BC28AF98A31896988A087D8870F8647857E84B583EC8323825A819180C88000",
--        INIT_01 => X"9830976A96A595DF95199452938C92C591FE913790708FA98EE18E198D528C8A",
--        INIT_02 => X"A462A3A2A2E0A21FA15DA09B9FD89F169E529D8F9CCB9C079B439A7F99BA98F5",
--        INIT_3E => X"7247717F70B76FF06F296E616D9A6CD46C0D6B476A8069BA68F5682F676A66A5",
--        INIT_3F => X"7ECF7E067D3D7C747BAC7AE37A1A7951788977C076F7762F7567749F73D6730E")    

        INIT_00 => x"8BC28AF98A31896988A087D8870F8647857E84B583EC8323825A819180C88000",
        INIT_01 => x"9830976A96A595DF95199452938C92C591FE913790708FA98EE18E198D528C8A",
        INIT_02 => x"A462A3A2A2E0A21FA15DA09B9FD89F169E529D8F9CCB9C079B439A7F99BA98F5",
        INIT_03 => x"B03BAF81AEC6AE0BAD4FAC93ABD6AB19AA5CA99EA8E0A821A762A6A3A5E3A523",
        INIT_04 => x"BB9DBAEBBA39B985B8D2B81DB768B6B3B5FDB547B490B3D8B320B268B1AFB0F5",
        INIT_05 => x"C66DC5C5C51CC472C3C8C31DC272C1C6C119C06CBFBEBF0FBE60BDB0BD00BC4F",
        INIT_06 => x"D08FCFF2CF55CEB7CE18CD78CCD8CC37CB95CAF2CA4FC9ABC907C861C7BBC714",
        INIT_07 => x"D9EAD95BD8CAD839D7A7D714D681D5ECD557D4C1D42AD392D2F9D260D1C5D12A",
        INIT_08 => x"E268E1E7E165E0E2E05FDFDADF54DECEDE46DDBEDD35DCAADC1FDB93DB06DA79",
        INIT_09 => x"E9F4E983E910E89DE829E7B3E73DE6C6E64DE5D4E55AE4DFE463E3E5E367E2E8",
        INIT_0A => x"F07AF01AEFB8EF56EEF2EE8DEE28EDC1ED59ECF0EC86EC1BEBAFEB41EAD3EA64",
        INIT_0B => x"F5ECF59DF54EF4FDF4AAF457F403F3ADF357F2FFF2A6F24CF1F1F195F138F0DA",
        INIT_0C => x"FA3BF9FFF9C2F983F943F902F8C0F87DF839F7F3F7ACF765F71CF6D1F686F63A",
        INIT_0D => x"FD5DFD34FD0AFCDEFCB2FC84FC54FC24FBF3FBC0FB8CFB57FB20FAE9FAB0FA76",
        INIT_0E => x"FF4BFF35FF1EFF06FEEDFED2FEB6FE99FE7BFE5CFE3BFE19FDF6FDD2FDACFD85",
        INIT_0F => x"FFFFFFFDFFF9FFF5FFEFFFE8FFE0FFD7FFCCFFC0FFB3FFA5FF95FF85FF73FF5F",
        INIT_10 => x"FF77FF89FF99FFA9FFB7FFC3FFCFFFD9FFE2FFEAFFF1FFF6FFFAFFFDFFFFFFFF",
        INIT_11 => x"FDB6FDDBFDFFFE22FE44FE64FE83FEA1FEBEFED9FEF3FF0DFF24FF3BFF50FF64",
        INIT_12 => x"FABFFAF7FB2FFB65FB99FBCDFC00FC31FC61FC90FCBDFCEAFD15FD3FFD68FD90",
        INIT_13 => x"F69AF6E5F72FF777F7BFF805F84BF88FF8D2F913F954F993F9D2FA0FFA4BFA86",
        INIT_14 => x"F150F1ADF209F264F2BDF316F36DF3C4F419F46DF4C0F512F562F5B2F600F64E",
        INIT_15 => x"EAF0EB5EEBCBEC37ECA1ED0BED74EDDCEE42EEA8EF0CEF70EFD2F033F093F0F2",
        INIT_16 => x"E388E406E483E4FFE57AE5F4E66DE6E5E75CE7D2E847E8BBE92EE9A0EA11EA81",
        INIT_17 => x"DB2BDBB8DC43DCCEDD58DDE1DE69DEF1DF77DFFCE081E104E187E209E28AE309",
        INIT_18 => x"D1EED288D321D3B9D451D4E8D57ED613D6A7D73BD7CDD85FD8F0D980DA0FDA9E",
        INIT_19 => x"C7E6C88CC931C9D6CA7ACB1DCBBFCC61CD02CDA2CE41CEE0CF7ED01BD0B7D153",
        INIT_1A => x"BD2EBDDEBE8EBF3DBFEBC099C146C1F3C29FC34AC3F4C49EC548C5F0C698C740",
        INIT_1B => x"B1DFB298B350B408B4BFB576B62CB6E2B797B84CB900B9B4BA67BB1ABBCCBC7D",
        INIT_1C => x"A615A6D5A794A853A911A9CFAA8DAB4AAC07ACC4AD80AE3CAEF7AFB1B06CB125",
        INIT_1D => x"99ED9AB29B769C3A9CFE9DC29E859F48A00BA0CDA18FA251A313A3D4A494A555",
        INIT_1E => x"8D868E4D8F158FDC90A4916B923292F993BF9486954C961296D8979E98639928",
        INIT_1F => x"80FD81C6828F8357842084E985B2867B8743880C88D4899D8A658B2D8BF68CBE",
        INIT_20 => x"7471753A760276CA7793785B792479ED7AB57B7E7C477D107DD97EA27F6B8034",
        INIT_21 => x"680268C8698E6A546B1A6BE06CA76D6D6E346EFC6FC3708A7152721972E173A9",
        INIT_22 => x"5BCF5C905D515E125ED45F976059611C61DF62A36366642B64EF65B36678673D",
        INIT_23 => x"4FF450AF5169522552E1539D545A551755D456925751580F58CE598E5A4E5B0E",
        INIT_24 => x"4490454245F546A8475C481148C6497B4A314AE84B9F4C574D0F4DC74E804F3A",
        INIT_25 => x"39BE3A663B0F3BB93C633D0E3DBA3E663F133FC0406E411D41CC427C432D43DE",
        INIT_26 => x"2F99303630D33171321032B0335133F23494353735DA367E372337C9386F3916",
        INIT_27 => x"263A26CA275A27EC287E291129A52A3A2ACF2B662BFD2C952D2E2DC72E622EFD",
        INIT_28 => x"1DB81E391EBC1F3F1FC3204820CE215421DC226522EE237924042490251D25AB",
        INIT_29 => x"1629169A170D178017F5186A18E1195819D11A4A1AC51B401BBD1C3A1CB91D38",
        INIT_2A => x"0F9D0FFE106010C31127118C11F2125912C1132B13951400146D14DA154915B8",
        INIT_2B => x"0A270A760AC60B180B6A0BBE0C120C680CBF0D170D700DCA0E250E820EDF0F3E",
        INIT_2C => x"05D30610064D068C06CC070E0750079407D8081E086508AD08F70941098D09D9",
        INIT_2D => x"02AC02D60300032C0359038703B703E80419044D048104B604ED0525055E0598",
        INIT_2E => x"00BA00D000E700FF011901340150016D018C01AC01CD01EF02120237025D0284",
        INIT_2F => x"000100030007000B001100190021002B00360042004F005E006E007F009100A5",
        INIT_30 => x"008300720061005300450038002D0023001A0013000D00080004000100000000",
        INIT_31 => x"023F021A01F701D401B3019301740156013A011F010500EC00D500BE00A90096",
        INIT_32 => x"053104F904C2048D0458042503F303C2039203630336030A02DF02B5028D0265",
        INIT_33 => x"0952090708BD0875082E07E807A3075F071C06DB069A065B061D05E005A5056A",
        INIT_34 => x"0E960E3A0DDE0D840D2A0CD20C7B0C250BD00B7C0B2A0AD80A880A3909EA099D",
        INIT_35 => x"14F21485141813AD134212D91270120911A2113D10D9107610140FB30F530EF4",
        INIT_36 => x"1C561BD81B5C1AE01A6519EC197318FB1884180F179A172616B3164215D11561",
        INIT_37 => x"24AF24232397230D228321FA217220EB20651FE01F5C1ED91E561DD51D541CD5",
        INIT_38 => x"2DE92D502CB72C1E2B872AF02A5B29C62932289E280C277A26EA265A25CB253D",
        INIT_39 => x"37EE374836A335FE355B34B83416337432D43234319530F630582FBB2F1F2E84",
        INIT_3A => x"42A341F3414440953FE73F393E8C3DE03D343C893BDE3B353A8B39E3393B3894",
        INIT_3B => x"4DF04D374C7F4BC84B104A5A49A348EE4839478446D0461C456944B744054354",
        INIT_3C => x"59B858F9583A577B56BC55FE5541548353C7530A524E519350D8501D4F634EA9",
        INIT_3D => x"65DF651A6456639262CE620A614760845FC25EFF5E3D5D7C5CBA5BF95B395A78",
        INIT_3E => x"7246717E70B66FEF6F286E606D996CD36C0C6B466A7F69B968F4682E676966A4",
        INIT_3F => x"7ECE7E057D3C7C737BAB7AE27A197950788877BF76F6762E7566749E73D5730D")

         
        port map (
        DO => D_out_L,                    -- Output read data port, width defined by READ_WIDTH parameter
        RDADDR => RDADDR,        -- Input address, width defined by port depth
        RDCLK => clk,                 -- 1-bit input clock
        RST => reset,                -- active high reset
        RDEN => '1',                -- read enable 
        REGCE => '1',                -- 1-bit input read output register enable - ignored
        DI => DI,                    -- Dummy write data - never used in this application
        WE => "00",                -- write to neither byte
        WRADDR => "0000000000",        -- Dummy place holder address
        WRCLK => clk,                -- 1-bit input write clock
        WREN => '0');                -- we are not writing to this RAM
        
    ACW : Audio_Codec_Wrapper port map(
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        ready => sw,
        L_bus_in => L_Bus_in, -- left channel input to DAC
        R_bus_in => R_Bus_in, -- right channel input to DAC
        L_bus_out => OPEN, -- left channel output from ADC
        R_bus_out => OPEN, -- right channel output from ADC
        scl => scl,
        sda => sda );
end behavior;