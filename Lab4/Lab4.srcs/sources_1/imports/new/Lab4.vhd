--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 7 Mar 17
-- Course:  ECE 383
-- File: Lab4.vhd
-- HW:  Lab4
--
-- Purp: Generate a waveform with the ability to change frequency.
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.lab4parts.all;
library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity Lab4 is
    Port ( clk : in  STD_LOGIC;
    reset_n : in  STD_LOGIC;
    ac_mclk : out STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
--    tmds : out STD_LOGIC_VECTOR (3 downto 0);
--    tmdsb : out STD_LOGIC_VECTOR (3 downto 0);
    btn : in STD_LOGIC_VECTOR(4 downto 0);
    ssw : in STD_LOGIC_VECTOR(7 downto 0));
end Lab4;

architecture behavior of Lab4 is

    signal sw : STD_LOGIC;
    signal cw : STD_LOGIC_VECTOR(6 downto 0);
    
begin

    datapath : Lab4_dp port map(
        clk => clk,
        reset_n => reset_n,
        ac_mclk => ac_mclk,
        ac_adc_sdata => ac_adc_sdata,
        ac_dac_sdata => ac_dac_sdata,
        ac_bclk => ac_bclk,
        ac_lrclk => ac_lrclk,
        scl => scl,
        sda => sda,
        ssw => ssw,
        sw => sw,
        cw => cw
    );
    
    controlUnit : Lab4_cu port map(
        clk => clk,
        reset_n => reset_n,
        btn => btn,
        sw => sw,
        cw => cw
    );

end behavior;