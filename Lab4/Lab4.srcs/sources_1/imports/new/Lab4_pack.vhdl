library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package lab4parts is

component Lab4
    Port ( clk : in  STD_LOGIC;
    reset_n : in  STD_LOGIC;
    ac_mclk : out STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;
--    tmds : out STD_LOGIC_VECTOR (3 downto 0);
--    tmdsb : out STD_LOGIC_VECTOR (3 downto 0);
    btn : in STD_LOGIC_VECTOR(4 downto 0);
    ssw : in STD_LOGIC_VECTOR(7 downto 0));
end component;

component Lab4_dp 
    Port ( clk : in  STD_LOGIC;
    reset_n : in  STD_LOGIC;
    ac_mclk : out STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC;    
--    tmds : out  STD_LOGIC_VECTOR (3 downto 0);
--    tmdsb : out  STD_LOGIC_VECTOR (3 downto 0);
    ssw : in std_logic_vector(7 downto 0);
    sw : out STD_LOGIC;
    cw : in std_logic_vector (6 downto 0));
end component;

component Lab4_cu
    Port ( clk : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    btn : in STD_LOGIC_VECTOR(4 downto 0);
    sw : in STD_LOGIC;
    cw : out STD_LOGIC_VECTOR(6 downto 0));
end component;

component Audio_Codec_Wrapper is
    Port ( clk : in STD_LOGIC;
    reset_n : in STD_LOGIC;
    ac_mclk : out STD_LOGIC;
    ac_adc_sdata : in STD_LOGIC;
    ac_dac_sdata : out STD_LOGIC;
    ac_bclk : out STD_LOGIC;
    ac_lrclk : out STD_LOGIC;
    ready : out STD_LOGIC;
    L_bus_in : in std_logic_vector(17 downto 0); -- left channel input to DAC
    R_bus_in : in std_logic_vector(17 downto 0); -- right channel input to DAC
    L_bus_out : out  std_logic_vector(17 downto 0); -- left channel output from ADC
    R_bus_out : out  std_logic_vector(17 downto 0); -- right channel output from ADC
    scl : inout STD_LOGIC;
    sda : inout STD_LOGIC);
end component;

end lab4parts;