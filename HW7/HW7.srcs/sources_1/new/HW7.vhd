--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 31 Jan 17
-- Course:  ECE 383
-- File: HW7.vhd
-- HW:  HW7
--
-- Purp: A FSM that increments a count after a button combination 
-- is pressed
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity HW7_top is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           btnl : in STD_LOGIC;
           btnr : in STD_LOGIC;
           count : out unsigned (3 downto 0));
end HW7_top;

architecture Behavioral of HW7_top is

component HW7_FSM is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           btnl : in STD_LOGIC;
           btnr : in STD_LOGIC;
           cnt_ctrl : out STD_LOGIC_VECTOR(1 downto 0));
end component;

component lec10 is
    Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			ctrl: in std_logic_vector(1 downto 0);
			D: in unsigned (3 downto 0);
			Q: out unsigned (3 downto 0));
end component;

signal cnt_ctrl : STD_LOGIC_VECTOR(1 downto 0) := "00";
signal count_sig : unsigned(3 downto 0) := "0000";

begin

    btn_FSM : HW7_FSM
    port map ( clk => clk, reset => reset, btnl => btnl, btnr => btnr, cnt_ctrl => cnt_ctrl );
    counter : lec10
    port map ( clk => clk, reset => reset, ctrl => cnt_ctrl, D => "0000", Q => count_sig );
    
    count <= count_sig;
    
end Behavioral;

-----------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity HW7_FSM is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           btnl : in STD_LOGIC;
           btnr : in STD_LOGIC;
           cnt_ctrl : out STD_LOGIC_VECTOR(1 downto 0));
end HW7_FSM;

architecture Behavioral of HW7_FSM is
    type state_type is (WaitBtnL, WaitBtnR, CountUp);
	signal state: state_type := WaitBtnL;
begin
    process(clk)
    begin
        if (rising_edge(clk)) then
            if(reset = '0') then
                state <= WaitBtnL;
            else
                case state is
                    when WaitBtnL =>
                        cnt_ctrl <= "00";
                        if btnl = '1' then state <= WaitBtnR;
                        end if;
                    when WaitBtnR =>
                        if btnr = '1' then state <= CountUp;
                        end if;
                    when CountUp =>
                        cnt_ctrl <= "01";
                        state <= WaitBtnL;
                end case;
            end if;
        end if;
    end process;
end Behavioral;

-------------------------------------------------------------------------------------------------------

library IEEE;		
use IEEE.std_logic_1164.all; 
use IEEE.NUMERIC_STD.ALL;
entity lec10 is
	Port(	clk: in  STD_LOGIC;
			reset : in  STD_LOGIC;
			ctrl: in std_logic_vector(1 downto 0);
			D: in unsigned (3 downto 0);
			Q: out unsigned (3 downto 0));
end lec10;

architecture behavior of lec10 is
	
	signal processQ: unsigned (3 downto 0) := (others => '0');

begin	
	-----------------------------------------------------------------------------
	--		crtl
	--		00			hold
	--		01			count up mod 10
	--		10			load D
	--		11			synch reset
	-----------------------------------------------------------------------------
	process(clk)
	begin
		if (rising_edge(clk)) then
			if (reset = '0') then
				processQ <= (others => '0');
			elsif (ctrl = "01") then
				processQ <= processQ + 1;
			elsif (ctrl = "10") then
				processQ <= unsigned(D);
			elsif (ctrl = "11") then
				processQ <= (others => '0');
			end if;
		end if;
	end process;
 
	Q <= processQ;
	
end behavior;