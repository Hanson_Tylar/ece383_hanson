--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 31 Jan 17
-- Course:  ECE 383
-- File: HW7_tb.vhd
-- HW:  HW7
--
-- Purp: Test functionality of HW7 entity
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity HW7_tb is
end HW7_tb;

architecture Behavioral of HW7_tb is

component HW7_top is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           btnl : in STD_LOGIC;
           btnr : in STD_LOGIC;
           count : out unsigned (3 downto 0));
end component;

signal clk, reset : STD_LOGIC := '0';
signal btnl, btnr : STD_LOGIC := '0';
signal count : unsigned (3 downto 0) := "0000";

-- Clock period definitions
constant clk_period : time := 10 ns;

begin

-- Instantiate the Unit Under Test (UUT)
    uut: HW7_top port map (
            clk => clk,
            reset => reset,
            btnl => btnl,
            btnr => btnr,
            count => count );
            
-- Clock process definitions
     clk_process :process
     begin
          clk <= '0';
          wait for clk_period/2;
          clk <= '1';
          wait for clk_period/2;
     end process;
     
     reset <= '1' after clk_period;
     btnl <= '1' after clk_period*2, '0' after clk_period*4, '1' after clk_period*6, '0' after clk_period*8, '1' after clk_period*20, '0' after clk_period*22;
     btnr <= '1' after clk_period*10, '0' after clk_period*12, '1' after clk_period*14, '0' after clk_period*16, '1' after clk_period*24, '0' after clk_period*26;   

end Behavioral;
