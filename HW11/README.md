# HW 11 

## By C2C Tylar Hanson

![](images/HW11.JPG)


![](images/HW11_1.JPG)

### 
Convert the following instructions into hexadecimal. 

- xori r18, r5, 3571
	- 101010 10010 00101  0000110111110011
	- AA450DF3

- rtid r15, -4230
	- 1111 0000 1000 0110 -> 0000 1111 0111 1010
	- 101101 10001 01111 0000111101111010
	- B62F0F7A

- lhui r7, r12, 234
	- 111001 00111 01100 0000000011101010
	- E4EC00EA
### Documentation
None