--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 12 Jan 17
-- Course:  ECE 383
-- File: mod5Counter.vhd
-- HW:  HW4
--
-- Purp: Implement a single mod 5 counter. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mod5Counter is
    Port ( clk, reset, ctrl : in STD_LOGIC;
       roll : out STD_LOGIC;
       Q : out unsigned(2 downto 0));
end mod5Counter;

architecture Behavioral of mod5Counter is
    signal processQ : unsigned(2 downto 0);    
begin
    process(clk)
    begin
        if (rising_edge(clk)) then 
            if (reset = '0') then
                processQ <= (others => '0');
            elsif ((ctrl = '1') and (processQ < 4)) then
                processQ <= processQ + 1;
            elsif ((ctrl = '1') and (processQ = 4)) then
                processQ <= (others => '0');
            end if;
        end if;
    end process;
    
   roll <= '1' when ((processQ = 4) and (ctrl = '1')) else '0';
   Q <= processQ;

end Behavioral;
