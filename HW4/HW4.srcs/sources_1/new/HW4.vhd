--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 12 Jan 17
-- Course:  ECE 383
-- File: HW4.vhd
-- HW:  HW4
--
-- Purp: Implement a two digit mod 5 counter. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity HW4 is
    Port ( clk, reset, ctrl : in STD_LOGIC;
           Q1, Q0 : out unsigned(2 downto 0));
end HW4;

architecture Behavioral of HW4 is  

component mod5Counter is
    Port ( clk, reset, ctrl : in STD_LOGIC;
           roll : out STD_LOGIC;
           Q : out unsigned(2 downto 0));
end component;
  
signal roll_sig : STD_LOGIC;

begin
    unit0: mod5Counter
    port map( clk => clk, reset => reset, ctrl => ctrl, roll => roll_sig, Q => Q0 );
    unit1: mod5Counter
    port map( clk => clk, reset => reset, ctrl => roll_sig, roll => OPEN, Q => Q1 );
    
end Behavioral;
