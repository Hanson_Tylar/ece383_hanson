--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 15 Jan 17
-- Course:  ECE 383
-- File: mod5Counter_tb.vhd
-- HW:  HW4
--
-- Purp: Test the implementation of a single mod 5 counter. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mod5Counter_tb is
end mod5Counter_tb;

architecture Behavioral of mod5Counter_tb is
    component mod5Counter is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           ctrl : in STD_LOGIC;
           roll : out STD_LOGIC;
           Q : out unsigned (2 downto 0));
   end component;
    
    signal clk_sig : STD_LOGIC := '0';
    signal reset_sig : STD_LOGIC := '0';
    signal ctrl_sig : STD_LOGIC := '0';
    
    signal roll_sig : STD_LOGIC;
    signal Q_sig : unsigned(2 downto 0);
    
    constant clk_period : time := 500 ns;
    
begin
    uut: mod5Counter port map (clk_sig, reset_sig, ctrl_sig, roll_sig, Q_sig);
    
    clk_process :process
       begin
            clk_sig <= '0';
            wait for clk_period/2;
            clk_sig <= '1';
            wait for clk_period/2;
       end process;
       
            reset_sig <= '1' after 10us; 
            ctrl_sig <= '1' after 20us;
            
end Behavioral;
