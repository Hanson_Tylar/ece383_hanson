--------------------------------------------------------------------
-- Name: Tylar Hanson
-- Date: 12 Jan 17
-- Course:  ECE 383
-- File: HW4_tb.vhd
-- HW:  HW4
--
-- Purp: Test the implementation of a two digit mod 5 counter. 
--
-- Doc: None
--
-- Academic Integrity Statement: I certify that, while others may have 
-- assisted me in brain storming, debugging and validating this program, 
-- the program itself is my own work. I understand that submitting code 
-- which is the work of other individuals is a violation of the honor   
-- code.  I also understand that if I knowingly give my original work to 
-- another individual is also a violation of the honor code. 
-------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity HW4_tb is
end HW4_tb;

architecture Behavioral of HW4_tb is
    component HW4
    Port ( clk, reset, ctrl : in STD_LOGIC;
           Q1, Q0 : out unsigned(2 downto 0));
    end component;
    
    signal clk_sig, reset_sig, ctrl_sig : STD_LOGIC := '0';
    signal Q1_sig, Q0_sig : unsigned(2 downto 0);
    constant clk_period : time := 500ns;
    
begin
    uut: HW4 port map (clk_sig,reset_sig,ctrl_sig,Q1_sig,Q0_sig);
            
    clk_process :process
    begin
         clk_sig <= '0';
         wait for clk_period/2;
         clk_sig <= '1';
         wait for clk_period/2;
    end process clk_process;

        reset_sig <= '1' after 1*clk_period;
        ctrl_sig <= '1' after 2*clk_period, '0' after 6*clk_period, '1' after 7*clk_period;
    
end architecture Behavioral;
